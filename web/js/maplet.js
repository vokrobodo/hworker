function initMap() {
        var center = {lat: 9.062957, lng: 7.399030};
        var l1 = {lat: 9.033574, lng: 7.482606};
        var l2 = {lat: 9.151621, lng: 7.339627};
        var l3 = {lat: 8.950532, lng: 7.058104};
        var l4 = {lat: 9.006836, lng: 7.564682};
        var l5 = {lat: 9.107219, lng: 7.402103};
		
        var map = new google.maps.Map(document.getElementById('mapid'), {
          zoom: 9,
          center: center
        });
        var marker = new google.maps.Marker({
          position: center,
          map: map
        });
        var marker = new google.maps.Marker({
          position: l1,
          map: map,
          'title': 'University of Abuja Teaching Hospital'
        });
        var marker = new google.maps.Marker({
          position: l2,
          map: map,
          'title': 'Wuse General Hospital'
        });
        var marker = new google.maps.Marker({
          position: l3,
          map: map,
          'title': 'Garki General Hospital'
        });
        var marker = new google.maps.Marker({
          position: l4,
          map: map,
          'title': 'Kelina Hospital'
        });
        var marker = new google.maps.Marker({
          position: l5,
          map: map,
          'title': 'Primus Super Specialty Hospital'
        });
		
      }