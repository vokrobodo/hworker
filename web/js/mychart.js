$(document).ready(function () {
    var bauchi = parseInt($('#bauchi').text())
    var niger = parseInt($('#niger').text())
    var rivers = parseInt($('#rivers').text())
    
    
    // Build the chart
    Highcharts.chart('homechart', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'State distribution of health workers'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y}</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Number',
            colorByPoint: true,
            data: [{
                name: 'Bauchi',
                y: bauchi
            }, {
                name: 'Niger',
                y: niger
            },{
                name: 'Rivers',
                y: rivers,
                sliced: true,
                selected: true
            },]
        }]
    });   
    
    
    
});


