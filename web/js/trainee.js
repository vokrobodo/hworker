
var app = new Vue({
    el: '#app',
    data: {
      stateName: 'ALL STATES IN NIGERIA',
      states:[],
      lgas:[],
      wards:[],
      trainees:[],
      selectedState:null,
      selectedLga:null,
      selectedWard:null,
      stateCount:0,
      lgaCount:0,
      wardCount:0,
      traineeCount:0,
      trainingList:[],
      trainedList:[],
      //countryTrainingStats:[],
      //genderTrainingStats:[]
    },
    created: function(){
        this.getStates();
        this.getTrainees();
        this.getTrainingList();
    },
    methods:{
        getTrainees: function(){
            var vm = this;
            axios.get('/hworker/trainee/list')
            .then(function(response){
                vm.trainees = response.data;
            })
            .catch(function(error){
                console.log(error);
            })
        },
        getStates: function(){
            var vm = this;
            axios.get('/hworker/api-states')
            .then(function(response){
                vm.states = response.data
            })
            .catch(function(err){
                console.log(err)
            })
        },
        getLgas: function(){
            var vm = this;
            axios.get('/hworker/api-lgas')
            .then(function(response){
                vm.lgas = response.data;
            })
            .catch(function(error){
                console.log(error)
            })
        },
        getStateLgas: function(evt){ //get's all LGAs belonging to selected state
            
            var stateId = evt.target.value
            this.selectedState = parseInt(stateId);
            var el = document.querySelector('#select_state');
            var stateName = el.options[el.selectedIndex].text;

            var vm = this;
            vm.stateName = stateName.toUpperCase() + ' STATE'; //set display name to this state's name
            axios.get('/hworker/lga/state/'+stateId)
            .then(function(response){
                vm.lgas = response.data;
                vm.wards = [];//clear wards list
            })
            .catch(function(error){
                console.log(error)
            })
            
            //re-draw chart
            var chartTitle = stateName + " State distribution of trained health workers ";
            this.drawChart(this.stateStats, chartTitle);
        },
        getLgaWards: function(evt){
            //switch off selectedState variable
            this.selectedState = null;

            var el = document.querySelector('#select_lga');
            var lgaName = el.options[el.selectedIndex].text;
            
            var lgaId = evt.target.value
            this.selectedLga = parseInt(lgaId);
            var vm = this;
            axios.get('/hworker/ward/lga/'+lgaId)
            .then(function(response){
                vm.wards = response.data;
            })
            .catch(function(error){
                console.log(error);
            })

            //redraw chart
            var chartTitle = lgaName + " LGA distribution of trained health workers"
            this.drawChart(this.lgaStats, chartTitle);
        },
        getWardFacilities: function(evt){
            //turn of selectedState and selectedLga Variables
            this.selectedState = null;
            this.selectedLga = null;
            this.selectedWard = parseInt(evt.target.value);

            var el = document.querySelector('#select_ward');
            var wardName = el.options[el.selectedIndex].text;

            //re-draw chart
            var chartTitle = wardName + " Health facility distribution of trained health workers"
            this.drawChart(this.wardStats, chartTitle);
        },
        getTrainingList: function(){
            var vm = this;
            axios.get('/hworker/training/training-list')
            .then(function (response) {
                vm.trainingList = response.data;
            })
            .catch(function (error) {
                console.log(error);
            })
        },
        distinct: function(value, index, self){ //this function gets all the distinct elements in an array
            return self.indexOf(value) === index;
        },
        getTrainedList: function(evt){
            var id = evt.target.value;
            var vm = this;
            axios.get('/hworker/training/trained/' +id)
                .then(function (response) {
                    vm.trainedList = response.data;
                })
                .catch(function (error) {
                    console.log(error);
                })
            //console.log(this.trainedList);
            this.drawTrainingDistributionChart(this.countryTrainingStats, "Training Distribution");
        },
        drawChart: function(cdata, chartTitle){

            Highcharts.chart('homechart', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: chartTitle
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y}</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true
                        },
                        showInLegend: true
                    }
                },
                series: [{
                    name: 'Number',
                    colorByPoint: true,
                    data: cdata
                }]
            });
            
        },
        drawTrainingDistributionChart: function (cdata, chartTitle) {

            var vm = this;
            Highcharts.chart('training-chart', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'column',

                },
                title: {
                    text: chartTitle
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y}</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true
                        },
                        showInLegend: true
                    }
                },
                series: [{
                    name: 'States',
                    colorByPoint: true,
                    data: cdata
                }]
            });
        }
    },
    computed: {
        computedTrainees: function(){
            var trainees = [];
            if(this.selectedState != null){
                for (var trainee of this.trainees){
                    if(this.selectedState == trainee.state_id){
                        trainees.push(trainee);
                    }
                }
                return trainees;
            }
            if(this.selectedLga != null){
                for (var trainee of this.trainees){
                    if(this.selectedLga == trainee.lga_id){
                        trainees.push(trainee);
                    }
                }
                return trainees;
            }
            if(this.selectedWard != null){
                for (var trainee of this.trainees){
                    if(this.selectedWard == trainee.ward_id){
                        trainees.push(trainee);
                    }
                }
                return trainees;
            }
            //draw chart
            var chartTitle = "Country Distribution of Health Workers";
            this.drawChart(this.countryStats, chartTitle)
            return this.trainees;
        },
        distinctStateNames: function(){
            var stateNames = this.trainees.map(function(trainee){
                return trainee.state_name;
            })
            return stateNames.filter(this.distinct);
        },
        distinctLgaNames: function(){
            var lgaNames = this.computedTrainees.map(function(trainee){
                return trainee.lga_name;
            })
            return lgaNames.filter(this.distinct);
        },
        distinctWardNames: function(){
            var wardNames = this.computedTrainees.map(function(trainee){
                return trainee.ward_name;
            })
            return wardNames.filter(this.distinct);
        },
        distinctFacilityNames: function(){
            var facilityNames = this.computedTrainees.map(function(trainee){
                return trainee.facility_name;
            })
            return facilityNames.filter(this.distinct);
        },
        distinctTrainingStateNames: function(){
            var stateNames = this.trainedList.map(function(trainee){
                return trainee.state_name;
            })
            return stateNames.filter(this.distinct);
        },

        statesTrainedCount: function(){
            var states = this.computedTrainees.map(function(trainee){
                return trainee.state_id;
            })
            var distinctStates = states.filter(this.distinct);//get distinct states
            return distinctStates.length;
        },
        lgasTrainedCount: function(){
            var lgas = this.computedTrainees.map(function(trainee){
                return trainee.lga_id;
            })
            var distinctLgas = lgas.filter(this.distinct);//get distinct states
            return distinctLgas.length;
        },
        wardsTrainedCount: function(){
            var wards = this.computedTrainees.map(function(trainee){
                return trainee.ward_id;
            })
            var distinctWards = wards.filter(this.distinct);//get distinct states
            return distinctWards.length;
        },
        totalTrainedCount: function(){
            return this.computedTrainees.length;
        },
        
        countryStats: function(){
            //get unique state names, get count for each state, place in object then
            var stats = [];
            for(var state of this.distinctStateNames){
                var count = 0;
                for(var trainee of this.trainees){
                    if(state == trainee.state_name){
                        count++;
                    }
                }
                stats.push({name:state, y:count});
            }
            return stats;
        },
        stateStats: function(){
            var stats = [];
            for (var lga of this.distinctLgaNames){
                var count = 0;
                for(var trainee of this.computedTrainees){
                    if(lga == trainee.lga_name){
                        count++;
                    }
                }
                stats.push({name:lga, y:count});
            }
            return stats;
        },
        lgaStats: function(){
            var stats = [];
            for(var ward of this.distinctWardNames){
                var count = 0;
                for(var trainee of this.computedTrainees){
                    if(ward == trainee.ward_name){
                        count++;
                    }
                }
                stats.push({name: ward, y:count});
            }
            return stats;
        },
        wardStats: function(){
            var stats = [];
            for(var facility of this.distinctFacilityNames){
                var count = 0;
                for(var trainee of this.computedTrainees){
                    if(facility == trainee.facility_name){
                        count++;
                    }
                }
                stats.push({name: facility, y:count});
            }
            return stats;
        },
        computedTrainedList: function () {
            return this.trainedList;
        },
        countryTrainingStats: function(){
            var stats = [];
            for(var state of this.distinctStateNames){
                var count = 0;
                for (var trainee of this.computedTrainedList ){
                    if(state == trainee.state_name){
                        count++;
                    }
                }
                stats.push({name: state, y: count});
            }
            return stats;
        }
    }
})


//High Charts




