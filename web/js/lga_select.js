$(document).ready(function(){
    //training form
    $('#training-state_id').change(function(){
        var state_id = $(this).val();
        
        $.post('/hworker/lga/get-state-lgas',{'state_id': state_id}, function(data){
            console.log(data);
            $('#training-lga_id').empty().append(data)
        })
    })
    
    //trainee form
    $('#trainee-state_id').change(function(){
        var state_id = $(this).val();
        
        $.post('/hworker/lga/get-state-lgas',{'state_id': state_id}, function(data){
            console.log(data);
            $('#trainee-lga_id').empty().append(data)
        })
    })
    
    //trainer form
    $('#trainer-state_id').change(function(){
        var state_id = $(this).val();
        
        $.post('/hworker/lga/get-state-lgas',{'state_id': state_id}, function(data){
            console.log(data);
            $('#trainer-lga_id').empty().append(data)
        })
    })
    
    //lga report
    $('#lgareportform-state_id').change(function(){
        var state_id = $(this).val();
        
        $.post('/hworker/lga/get-state-lgas',{'state_id': state_id}, function(data){
            console.log(data);
            $('#lgareportform-lga_id').empty().append(data)
        })
    })
    
    //healthfacility
    $('#healthfacility-state_id').change(function(){
        var state_id = $(this).val();
        
        $.post('/hworker/lga/get-state-lgas',{'state_id': state_id}, function(data){
            console.log(data);
            $('#healthfacility-lga_id').empty().append(data)
        })
    })
    
    //training plans
    $('#trainingplan-state_id').change(function(){
        var state_id = $(this).val();
        
        $.post('/hworker/lga/get-state-lgas',{'state_id': state_id}, function(data){
            console.log(data);
            $('#trainingplan-lga_id').empty().append(data)
        })
    })
    
    //trainee
    $('#trainee-state_id').change(function(){
        var state_id = $(this).val();
        
        $.post('/hworker/lga/get-state-lgas',{'state_id': state_id}, function(data){
            console.log(data);
            $('#trainee-lga_id').empty().append(data)
        })
    })
})


