<?php

namespace app\controllers;

use Yii;
use app\models\Trainee;
use app\models\TraineeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Training;
use app\models\PhotoUploadForm;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * TraineeController implements the CRUD actions for Trainee model.
 */
class TraineeController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Trainee models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TraineeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $trained = Trainee::find()->count();
        $query = new \yii\db\Query();
        $rows = $query->select('lga_id')->from('trainee')->distinct();
        $lgacount = count($rows->all());
        $ward_count = Trainee::find()->select('ward_id')->distinct()->count();

        $states = Trainee::find()->select('state_id')->distinct()->all();
        $s_distribution = [];
        $state_names = [];
        foreach($states as $s){
            $arr = [];
            $state = \app\models\State::getStateName($s->state_id);

            //required for chart coordinates
            $arr['name'] = $state;
            $arr['y'] = intval(Trainee::find()->where(['state_id' => $s->state_id])->count());

            //required for displaying markers on map
            $state_names[] = $state.', Nigeria';//this is for the map

            array_push($s_distribution, $arr);
        }
        $state_distribution = json_encode($s_distribution);
        $state_names = json_encode($state_names); //encode state names

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'trained' => $trained,
            'lgacount' => $lgacount,
            'ward_count' => $ward_count,
            'state_distribution' => $state_distribution,
            'state_names' => $state_names
        ]);
    }

    /**
     * Displays a single Trainee model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'training'=> \app\models\TraineeTraining::find()->where(['trainee_id'=>$id])->all()
        ]);
    }

    /**
     * Creates a new Trainee model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed

    public function actionCreate()
    {
        $model = new Trainee();
        $training = new \app\models\TraineeTraining();
        $upload = new PhotoUploadForm();
        $upload->scenario = PhotoUploadForm::SCENARIO_CREATE;

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post()) && $model->validate() && $training->load(Yii::$app->request->post()) && $training->validate()) {
            $upload->photo = UploadedFile::getInstance($upload, 'photo');
            $upload->upload();

            $model->photo = $upload->dbName;
            $model->save();
            foreach ($training->training_id as $t){
                $tt = new \app\models\TraineeTraining();
                $tt->trainee_id = $model->id;
                $tt->training_id = $t;
                $tt->save(false);
            }
            Yii::$app->session->setFlash('success_message',"The record has been created successfully");
            return $this->redirect(['view', 'id' => $model->id]);


    } else {
        return $this->render('create', [
            'model' => $model,
            'training' => $training,
            'upload' => $upload
        ]);
    }
    */

    /**
     * Creates a new Trainee model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Trainee();
        $training = new \app\models\TraineeTraining();

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $training->load(Yii::$app->request->post()) && $training->validate()) {

            $model->save();
            foreach ($training->training_id as $t){
                $tt = new \app\models\TraineeTraining();
                $tt->trainee_id = $model->id;
                $tt->training_id = $t;
                $tt->save(false);
            }
            Yii::$app->session->setFlash('success_message',"The record has been created successfully");
            return $this->redirect(['view', 'id' => $model->id]);


    } else {
        return $this->render('create', [
            'model' => $model,
            'training' => $training,
        ]);
    }



    }

    /**
     * Updates an existing Trainee model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     *
    public function actionUpdate($id)
    {
        $training = [];
        $upload = new PhotoUploadForm();
        $upload->scenario = PhotoUploadForm::SCENARIO_UPDATE;
        $model = $this->findModel($id);
        $trainings = \app\models\TraineeTraining::find()->where(['trainee_id'=>$id])->indexBy('id')->all();
        foreach($trainings as $t){
            $training[$t->training_id] = ['SELECTED'=>'SELECTED'];
        }
        $train = new \app\models\TraineeTraining;

        if (Yii::$app->request->isPost) {
            $upload->photo = UploadedFile::getInstance($upload, 'photo');
            if($upload->photo != NULL && $upload->upload()){
                if ($model->load(Yii::$app->request->post()) && $model->validate() && $train->load(Yii::$app->request->post()) && $train->validate()) {
                    $model->photo = $upload->dbName;
                    $model->save();

                    //delete traineetraining models and save again
                    \app\models\TraineeTraining::deleteAll(['trainee_id'=>$id]);
                    foreach ($train->training_id as $t){
                        $tt = new \app\models\TraineeTraining();
                        $tt->trainee_id = $model->id;
                        $tt->training_id = $t;
                        $tt->save(false);
                    }
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }elseif($upload->photo == NULL){
                if ($model->load(Yii::$app->request->post()) && $model->validate() && $train->load(Yii::$app->request->post()) && $train->validate()) {

                    $model->save();

                    //delete traineetraining models and save again
                    \app\models\TraineeTraining::deleteAll(['trainee_id'=>$id]);
                    foreach ($train->training_id as $t){
                        $tt = new \app\models\TraineeTraining();
                        $tt->trainee_id = $model->id;
                        $tt->training_id = $t;
                        $tt->save(false);
                    }
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        }else {
            return $this->render('update', [
                'model' => $model,
                'training'=> $training,
                'train'=>$train,
                'upload' => $upload
            ]);
        }
    }
    */

    /**
     * Updates an existing Trainee model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $training = [];
        $model = $this->findModel($id);
        $trainings = \app\models\TraineeTraining::find()->where(['trainee_id'=>$id])->indexBy('id')->all();
        foreach($trainings as $t){
            $training[$t->training_id] = ['SELECTED'=>'SELECTED'];
        }
        $train = new \app\models\TraineeTraining;

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $train->load(Yii::$app->request->post()) && $train->validate()) {
            $model->photo = $upload->dbName;
            $model->save();

            //delete traineetraining models and save again
            \app\models\TraineeTraining::deleteAll(['trainee_id'=>$id]);
            foreach ($train->training_id as $t){
                $tt = new \app\models\TraineeTraining();
                $tt->trainee_id = $model->id;
                $tt->training_id = $t;
                $tt->save(false);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }
        else {
            return $this->render('update', [
                'model' => $model,
                'training'=> $training,
                'train'=>$train,
            ]);
        }
    }

    /**
     * Deletes an existing Trainee model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Trainee model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Trainee the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Trainee::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionStateReportForm()
    {
        $model = new \app\models\StateReportForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            return $this->redirect(['state-report-view', 'id' => $model->state_id]);
        } else {
            return $this->render('state_report', [
                'model'=>$model,
            ]);
        }

    }

    public function actionStateReportView($id)
    {
        $trainees = Trainee::find()->where(['state_id' => $id])->all();
        $report = true;

        if(count($trainees) == 0){
            throw new NotFoundHttpException('The are no trainees in the selected state');
        }
        $lga_count = Trainee::find()->select('lga_id')->distinct()->where(['state_id'=>$id])->count();
        $ward_count = Trainee::find()->select('ward_id')->distinct()->where(['state_id'=>$id])->count();

        //get the distribution per LGA
        $lgas = Trainee::find()->select('lga_id')->distinct()->where(['state_id'=>$id])->all();
        $l_distribution = [];
        foreach($lgas as $l){
            $arr = [];
            $name = \app\models\Lga::getLgaName($l->lga_id);
            $count = intval(Trainee::find()->where(['lga_id'=>$l->lga_id])->count());
            array_push($arr, $name);
            array_push($arr, $count);

            array_push($l_distribution, $arr);
        }
        $lga_distribution = json_encode($l_distribution);
        //var_dump($lga_distribution);

        return $this->render('state_report', [
                'trainees'=> $trainees,
                'report' => $report,
                'lga_count' => $lga_count,
                'ward_count' => $ward_count,
                'state_id' => $id,
                'lga_distribution' => $lga_distribution
        ]);

    }

    public function actionLgaReportForm()
    {
        $model = new \app\models\LgaReportForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            return $this->redirect(['lga-report-view', 'id' => $model->lga_id]);
        } else {
            return $this->render('lga_report', [
                'model'=>$model,
            ]);
        }
    }

    public function actionLgaReportView($id)
    {
        $trainees = Trainee::find()->where(['lga_id' => $id])->all();
        $report = true;

        if(count($trainees) == 0){
            throw new NotFoundHttpException('The are no trainees in the selected LGA');
        }
        $ward_count = Trainee::find()->select('ward_id')->distinct()->where(['lga_id'=>$id])->count();

        //get the distribution per ward
        $wards = Trainee::find()->select('ward_id')->distinct()->where(['lga_id'=>$id])->all();
        $w_distribution = [];
        foreach($wards as $w){
            $arr = [];
            $name = \app\models\Ward::getWardName($w->ward_id);
            $cname = str_replace(["\r\n",'?',"\\"], "", $name);//clean out newline caharacters
            $count = intval(Trainee::find()->where(['ward_id'=>$w->ward_id])->count());
            array_push($arr, $cname);
            array_push($arr, $count);

            array_push($w_distribution, $arr);
        }
        $ward_distribution = json_encode($w_distribution);
        //var_dump($ward_distribution);
        //return;

        return $this->render('lga_report', [
                'trainees'=> $trainees,
                'report' => $report,
                'ward_count' => $ward_count,
                'ward_distribution' => $ward_distribution,
                'lga_id' => $id,
        ]);

    }

    /**
     * Lists all Trainer models.
     * @return mixed
     */
    public function actionDirectory()
    {
        $searchModel = new TraineeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('directory', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /*
     * Get count of states where training has taken place
    */
    public function actionStateCount()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $trainees = Trainee::find()->all();
        $states = [];
        foreach($trainees as $trainee){
            $states[] = $trainee->state_id;
        }
        $result = array_unique($states);
        $data = ['count' => count($result)];
        return $data;
    }

    /*
     * Get count of lgas where training has taken place
    */
    public function actionLgaCount()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $trainees = Trainee::find()->all();
        $lgas = [];
        foreach($trainees as $trainee){
            $lgas[] = $trainee->lga_id;
        }
        $result = array_unique($lgas);
        $data = ['count' => count($result)];
        return $data;
    }

    /*
     * Get count of wards where training has taken place
    */
    public function actionWardCount()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $trainees = Trainee::find()->all();
        $wards = [];
        foreach($trainees as $trainee){
            $wards[] = $trainee->ward_id;
        }
        $result = array_unique($wards);
        $data = ['count' => count($result)];
        return $data;
    }

    /*
     * Get count of trainess where training has taken place
    */
    public function actionTraineeCount()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $trainees = Trainee::find()->all();
        $data = ['count' => count($trainees)];
        return $data;
    }

    public function actionList()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $lgas = Trainee::find()->all();
        $data = ArrayHelper::toArray($lgas, [
            'app\models\Trainee' => [
                'id',
                'surname',
                'firstname',
                'middlename',
                'phone',
                'email',
                'gender',
                'state_id',
                'state_name' => function($model){
                    return \app\models\State::getStateName($model->state_id);
                },
                'lga_id',
                'lga_name' => function($model){
                    return \app\models\Lga::getLgaName($model->lga_id);
                },
                'ward_id',
                'ward_name' => function($model){
                    return \app\models\Ward::getWardName($model->ward_id);
                },
                'facility_id',
                'facility_name' => function($model){
                    return \app\models\Healthfacility::getName($model->facility_id);
                }
            ]
        ]);
        return $data;
    }

}
