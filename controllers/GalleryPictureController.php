<?php

namespace app\controllers;

use Yii;
use app\models\GalleryPicture;
use app\models\GalleryPictureSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\GalleryPhotoForm;
use app\models\Gallery;
use yii\web\HttpException;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * GalleryPictureController implements the CRUD actions for GalleryPicture model.
 */
class GalleryPictureController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all GalleryPicture models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GalleryPictureSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single GalleryPicture model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new GalleryPicture model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $gallery_id = '';
        if (($gallery = Gallery::findOne($id)) == null) {
            throw new HttpException(404, 'The requested page does not exist.');
        }else{
            $gallery_id = $gallery->id;
        }
        
        $model = new GalleryPicture();
        $upload = new GalleryPhotoForm();
        $upload->scenario = GalleryPhotoForm::SCENARIO_CREATE;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $upload->photo = UploadedFile::getInstance($upload, 'photo');
            if($upload->upload()){//if file was successfully uploaded
                $model->picture = $upload->dbName;
                $model->save(FALSE);
            }
            return $this->redirect(['gallery/view', 'id' => $model->gallery_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'upload' => $upload,
                'gallery_id' => $gallery_id
            ]);
        }
    }

    /**
     * Updates an existing GalleryPicture model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing GalleryPicture model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $gallery_id = $model->gallery_id;
        unlink(Yii::$app->basePath.'/'.GalleryPhotoForm::GALLERY_FOLDER.$model->picture); //delete old file
        $model->delete();

        return $this->redirect(['/gallery/view','id'=>$gallery_id]);
    }

    /**
     * Finds the GalleryPicture model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return GalleryPicture the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = GalleryPicture::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
