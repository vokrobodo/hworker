<?php

namespace app\controllers;

use app\models\Resource;
use app\models\Trainee;
use app\models\TraineeTraining;
use app\models\TrainingComment;
use app\models\TrainingResource;
use Yii;
use app\models\Training;
use app\models\TrainingSearch;
use yii\base\Object;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * TrainingController implements the CRUD actions for Training model.
 */
class TrainingController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Training models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TrainingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Training model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $resources = [];
        $resource_ids = [];

        $comments = [];

        $training_resources = TrainingResource::find()->where(['training_id' => $id])->all();
        $comments = TrainingComment::find()->where(['training_id' => $id])->all();

        if(count($training_resources)  > 0){
            foreach ($training_resources as $tr){
                $resource_ids[] = $tr->resource_id;
            }
            $resources = Resource::findAll($resource_ids);
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
            'resources' => $resources,
            'comments' => $comments,
        ]);
    }

    /**
     * Creates a new Training model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Training();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->training_id = Training::generateTrainingCode();
            $model->save(FALSE);
            Yii::$app->session->setFlash('success_message',"The record has been created successfully");
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Training model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Training model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Training model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Training the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Training::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionTrainingList()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $trainings = Training::find()->all();
        $data = ArrayHelper::toArray($trainings, [
            'app\models\Training' => [
                'id',
                'training_id',
                'title',
            ]
        ]);
        return $data;
    }

    /*
     * gets a count of trainees ordered by state
     * for the selected training
     */
    public function actionCountryTrainingStats($id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $trainings = TraineeTraining::find()->where(['training_id' => $id])->all();
        $states = [];
        $unique_states = [];

        $data = [];
        foreach($trainings as $tr){
            $state_id = Trainee::findOne($tr->trainee_id)->state_id;
            $state_name = strval(\app\models\State::getStateName($state_id));
            $states[] = $state_name;
        }
        $unique_states = array_unique($states);
        $unique_states = array_filter($unique_states, 'strlen');//remove empty entries from array

        foreach($unique_states as $us){
            $count = 0;
            foreach($states as $s){
                if($us == $s){
                    $count++;
                }
            }
            $d[$us] = [$count];
        }

        return $data;

    }

    /*
     * gets a count of trainees ordered by gender
     * for the selected training
     */
    public function actionGenderTrainingStats()
    {

    }

    public function actionTrained($id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $trainings = TraineeTraining::find()->where(['training_id' => $id])->all();
        $data = ArrayHelper::toArray($trainings, [
            'app\models\TraineeTraining' => [
                'id',
                'trainee_id',
                'training_id',
                'state_name' => function($model){
                    $state_id = Trainee::findOne($model->trainee_id)->state_id;
                    return \app\models\State::getStateName($state_id);
                },
            ]
        ]);
        return $data;
    }

}
