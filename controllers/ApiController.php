<?php

namespace app\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\Trainee;
use yii\web\Controller;
use yii\filters\auth\HttpBasicAuth;
/**
 * Description of ApiController
 *
 * @author Victor
 */
class ApiController extends Controller{
    //put your code here
    public $response;
    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();
        $this->response = Yii::$app->response;
        $this->response->format = \yii\web\Response::FORMAT_JSON;
    }
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actionTrainees()
    {               
        $trainees = Trainee::find()
                ->select(['surname','firstname','email','phone'])
                ->all();
        $this->response->data = $trainees;
    }
    
    public function actionTrainee($id=1)
    {         
        $trainee = Trainee::findOne($id);
        $this->response->data = $trainee;
    }
    
    
}
