<?php

namespace app\controllers;

use Yii;
use yii\rest\ActiveController;
use app\models\Trainee;
use yii\data\ActiveDataProvider;
use yii\data\ActiveDataFilter;
use app\models\State;

class ApiTraineeController extends ActiveController
{
    public $modelClass = 'app\models\Trainee';

    public function actions(){
        $actions = parent::actions();
        unset($actions['index']);
        //unset($actions['state']);
        return $actions;
    }
    
    public function actionIndex(){
        $activeData = new ActiveDataProvider([
            'query' => Trainee::find(),
            'pagination' => false
        ]);
        return $activeData;
    }

    
    
}