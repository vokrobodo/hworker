<?php

namespace app\controllers;

use Yii;
use app\models\Resource;
use app\models\ResourceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\ResourceUploadForm;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * ResourceController implements the CRUD actions for Resource model.
 */
class ResourceController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Resource models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ResourceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Resource model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Resource model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Resource();
        $upload = new ResourceUploadForm();
        $upload->scenario = ResourceUploadForm::SCENARIO_CREATE;

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post()) && $model->validate()) {
            $upload->file = UploadedFile::getInstance($upload, 'file'); //get uploaded file instance
            $upload->upload(); //either uploads file or throws a 404 error
            $model->filename = $upload->dbName;
            $model->save(FALSE); //validation already done
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'upload' => $upload
            ]);
        }
    }

    /**
     * Updates an existing Resource model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $upload = new ResourceUploadForm();
        $upload->scenario = ResourceUploadForm::SCENARIO_UPDATE;
        
        $oldfile = $model->filename;
        
        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post()) && $model->validate()){ //if form is submitted
            $upload->file = UploadedFile::getInstance($upload, 'file'); //get uploaded file instance
            if($upload->file != NULL){//file exists
                $upload->upload();//upload new file
                unlink(Yii::$app->basePath.'/'. ResourceUploadForm::RESOURCE_FOLDER.$oldfile); //delete old file
                $model->filename = $upload->dbName; //add uploaded file name to model
            }
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }else {
            return $this->render('update', [
                'model' => $model,
                'upload' => $upload
            ]);
        }
    }

    /**
     * Deletes an existing Resource model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        unlink(Yii::$app->basePath.'/'. ResourceUploadForm::RESOURCE_FOLDER.$model->filename); //delete old file
        $model->delete();
        
        return $this->redirect(['index']);
    }

    /**
     * Finds the Resource model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Resource the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Resource::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionViewCategories()
    {
        $categories = \app\models\ResourceCategory::find()->all();
        
        return $this->render('resource_categories',[
            'categories' => $categories
        ]);
    }
    
    public function actionViewCategoryResources($id)
    {
        $resources = Resource::find()->where(['category_id' => $id])->all();
        if($resources == NULL){
            throw new NotFoundHttpException('The requested page does not exist');
        }
        $category = \app\models\ResourceCategory::findOne($id); 
        return $this->render('resources',[
            'resources' => $resources,
            'category_name' => $category->name
        ]);
    }

    public function actionGetByCategory()
    {
        $options = "";
        $catid = Yii::$app->request->get('catid');
        $resources = Resource::find()->where(['category_id' => $catid])->all();


        if(count($resources) == 0){
            echo "Nothing";
        }else{
            foreach ($resources as $resource){
                $options.= "<option value=$resource->id>$resource->name</option>";
            }
            echo $options;
        }
    }
}
