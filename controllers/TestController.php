<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\Ward;
use app\models\Healthfacility;

/**
 * Description of TestController
 *
 * @author Victor
 */
class TestController extends Controller{
    
    public function actionWards()
    {
        $wards = Ward::find()->all();
        
        return $this->render('ward',['wards'=>$wards]);
    }
    
    public function actionFacilities()
    {
        $facilities = Healthfacility::find()->all();
        
        return $this->render('facilities',['facilities'=>$facilities]);
    }
}
