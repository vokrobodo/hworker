<?php

namespace app\controllers;

use Yii;
use yii\rest\ActiveController;
use app\models\State;
use yii\data\ActiveDataProvider;

class ApiStateController extends ActiveController
{
    public $modelClass = 'app\models\State';

    public function actions(){
        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
    }
    
    public function actionIndex(){
        $activeData = new ActiveDataProvider([
            'query' => State::find(),
            'pagination' => false
        ]);
        return $activeData;
    }
}