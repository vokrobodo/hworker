<?php

namespace app\controllers;

use Yii;
use app\models\Trainer;
use app\models\TrainerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\PhotoUploadForm;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * TrainerController implements the CRUD actions for Trainer model.
 */
class TrainerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Trainer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TrainerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Trainer model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'training'=> \app\models\TrainerTraining::find()->where(['trainer_id'=>$id])->all()
        ]);
    }

    /**
     * Creates a new Trainer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Trainer();
        $training = new \app\models\TrainerTraining();
//        $upload = new PhotoUploadForm();
//        $upload->scenario = PhotoUploadForm::SCENARIO_CREATE;
//
//        if (Yii::$app->request->isPost) {
//            $upload->photo = UploadedFile::getInstance($upload, 'photo');
//            if ($upload->upload()) {
//                if ( $model->load(Yii::$app->request->post()) && $model->validate() && $training->load(Yii::$app->request->post()) && $training->validate()) {
//                    $model->photo = $upload->dbName;
//                    $model->save();           
//                    foreach ($training->training_id as $t){
//                        $tt = new \app\models\TrainerTraining();
//                        $tt->trainer_id = $model->id;
//                        $tt->training_id = $t;
//                        $tt->save(false);
//                    }
//            
//                Yii::$app->session->setFlash('success_message','The record has been created successfully');
//                return $this->redirect(['view', 'id' => $model->id]);
//                } 
//            }
//        } 
        if ( $model->load(Yii::$app->request->post()) && $model->validate() && $training->load(Yii::$app->request->post()) && $training->validate()) {
            
            $model->save();           
            foreach ($training->training_id as $t){
                $tt = new \app\models\TrainerTraining();
                $tt->trainer_id = $model->id;
                $tt->training_id = $t;
                $tt->save(false);
            }
            Yii::$app->session->setFlash('success_message',"The record has been created successfully");
            return $this->redirect(['view', 'id' => $model->id]);
        }else {
            return $this->render('create', [
                'model' => $model,
                //'upload' => $upload,
                'training' => $training,
            ]);
        }
    }

    /**
     * Updates an existing Trainer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $training = [];
        $upload = new PhotoUploadForm();
        $upload->scenario = PhotoUploadForm::SCENARIO_UPDATE;
        $training = [];
        $train = new \app\models\TrainerTraining;
        $trainings = \app\models\TrainerTraining::find()->where(['trainer_id'=>$id])->indexBy('id')->all();
        
        
        foreach($trainings as $t){
            $training[$t->training_id] = ['SELECTED'=>'SELECTED'];
        }
        

        if (Yii::$app->request->isPost) {
            $upload->photo = UploadedFile::getInstance($upload, 'photo');
            if($upload->photo != NULL && $upload->upload()){
                if ($model->load(Yii::$app->request->post()) && $model->validate() && $train->load(Yii::$app->request->post()) && $train->validate()) {
                    $model->photo = $upload->dbName;
                    $model->save();

                    //delete traineetraining models and save again
                    \app\models\TrainerTraining::deleteAll(['trainer_id'=>$id]);
                    foreach ($train->training_id as $t){
                        $tt = new \app\models\TrainerTraining();
                        $tt->trainer_id = $model->id;
                        $tt->training_id = $t;
                        $tt->save(false);
                    }
                    return $this->redirect(['view', 'id' => $model->id]);
                } 
            }elseif($upload->photo == NULL){
                if ($model->load(Yii::$app->request->post()) && $model->validate() && $train->load(Yii::$app->request->post()) && $train->validate()) {
                    
                    $model->save();

                    //delete traineetraining models and save again
                    \app\models\TrainerTraining::deleteAll(['trainer_id'=>$id]);
                    foreach ($train->training_id as $t){
                        $tt = new \app\models\TrainerTraining();
                        $tt->trainer_id = $model->id;
                        $tt->training_id = $t;
                        $tt->save(false);
                    }
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        }else {
            return $this->render('update', [
                'model' => $model,
                'training'=> $training,
                'train'=>$train,
                'upload' => $upload,
                'trainings' => $trainings
            ]);
        }
    }

    /**
     * Deletes an existing Trainer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Trainer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Trainer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Trainer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionStateReportForm()
    {
        $model = new \app\models\StateReportForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            return $this->redirect(['state-report-view', 'id' => $model->state_id]);
        } else {
            return $this->render('state_report', [
                'model'=>$model,
            ]);
        }
        
    }
    
    public function actionStateReportView($id)
    {
        $trainers = Trainer::find()->where(['state_id' => $id])->all();
        $report = true;
        
        if(count($trainers) == 0){
            throw new NotFoundHttpException('The are no trainers in the selected state');
        }
        $lga_count = Trainer::find()->select('lga_id')->distinct()->where(['state_id'=>$id])->count();
        
        return $this->render('state_report', [
                'trainers'=> $trainers,
                'report' => $report,
                'lga_count' => $lga_count,
                'state_id' => $id
        ]);
        
    }
    
    public function actionLgaReportForm()
    {
        $model = new \app\models\LgaReportForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            return $this->redirect(['lga-report-view', 'id' => $model->lga_id]);
        } else {
            return $this->render('lga_report', [
                'model'=>$model,
            ]);
        }
    }
    
    public function actionLgaReportView($id)
    {
        $trainers = Trainer::find()->where(['lga_id' => $id])->all();
        $report = true;
        
        if(count($trainers) == 0){
            throw new NotFoundHttpException('The are no trainers in the selected LGA');
        }        
        
        return $this->render('lga_report', [
                'trainers'=> $trainers,
                'report' => $report,
        ]);
        
    }
    
    
}
