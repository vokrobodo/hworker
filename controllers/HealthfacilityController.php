<?php

namespace app\controllers;

use app\models\Trainee;
use app\models\TraineeTraining;
use app\models\Training;
use Yii;
use app\models\Healthfacility;
use app\models\HealthfacilitySearch;
use yii\db\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * HealthfacilityController implements the CRUD actions for Healthfacility model.
 */
class HealthfacilityController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Healthfacility models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HealthfacilitySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Healthfacility model.
     * @param integer $id
     * @return mixed
     */

    public function actionView($id)
    {
        $training_counts = Trainee::getTrainingsByFacility($id);

        //print_r($training_counts);
        //return;

        return $this->render('view', [
            'model' => $this->findModel($id),
            'training_counts' => $training_counts
        ]);

    }

    /**
     * Creates a new Healthfacility model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Healthfacility();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success_message',"The record has been created successfully");
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Healthfacility model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Healthfacility model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Healthfacility model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Healthfacility the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Healthfacility::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionTraineeList($id, $training_id)
    {
        $train = Training::findOne($training_id);
        $facility = Healthfacility::findOne($id);

        if(empty($train)){
            throw new NotFoundHttpException('The training does not exist');
        }

        if(empty($facility)){
            throw new NotFoundHttpException('The facility does not exist');
        }

        $trainees = Trainee::getTraineesByFacility($id, $training_id);

        return $this->render('training_list',
        [
            'trainees' => $trainees,
        ]);
    }
}
