<?php

namespace app\controllers;

use app\models\Training;
use Yii;
use app\models\TrainingComment;
use app\models\TrainingCommentSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TrainingCommentController implements the CRUD actions for TrainingComment model.
 */
class TrainingCommentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TrainingComment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TrainingCommentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TrainingComment model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TrainingComment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $training = Training::findOne($id);

        if($training == null){
            throw new NotFoundHttpException('The training specified does not exist');
        }

        $comments = TrainingComment::find()->where(
            [
                'training_id' => $id,
                'user_id' => Yii::$app->user->getId()
            ])->all();

        if(count($comments) > 0){
            throw new ForbiddenHttpException('You have already commented on this training');
        }

        $model = new TrainingComment();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->user_id = Yii::$app->user->id;
            $model->save();
            return $this->redirect(['/training/view', 'id' => $model->training_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'training_id' => $training->id,
                'training_title' => $training->title,
            ]);
        }
    }

    /**
     * Updates an existing TrainingComment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $training = Training::findOne($model->training_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/training/view', 'id' => $model->training_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'training_id' => $training->id,
                'training_title' => $training->title,
            ]);
        }
    }

    /**
     * Deletes an existing TrainingComment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $training_id = $model->training_id;
        $model->delete();

        return $this->redirect(['/training/view', 'id' => $training_id]);
    }

    /**
     * Finds the TrainingComment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TrainingComment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TrainingComment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
