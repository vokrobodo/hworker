<?php

namespace app\controllers;

use Yii;
use yii\rest\ActiveController;
use app\models\Ward;
use yii\data\ActiveDataProvider;

class ApiWardController extends ActiveController
{
    public $modelClass = 'app\models\Ward';

    public function actions(){
        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
    }
    
    public function actionIndex(){
        $activeData = new ActiveDataProvider([
            'query' => Ward::find(),
            'pagination' => false
        ]);
        return $activeData;
    }
}