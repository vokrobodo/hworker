<?php

namespace app\controllers;

use Yii;
use yii\rest\ActiveController;
use app\models\Lga;
use yii\data\ActiveDataProvider;
use yii\data\ActiveDataFilter;

class ApiLgaController extends ActiveController
{
    public $modelClass = 'app\models\Ward';

    public function actions(){
        $actions = parent::actions();
        unset($actions['index']);
        //unset($actions['state']);
        return $actions;
    }
    
    public function actionIndex(){
        $activeData = new ActiveDataProvider([
            'query' => Lga::find(),
            'pagination' => false
        ]);
        return $activeData;
    }

    
    
}