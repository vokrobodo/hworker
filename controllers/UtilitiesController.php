<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
/**
 * Description of Utilities
 *
 * @author Victor
 */
class UtilitiesController extends Controller{
    
    public function actionIndex()
    {
        echo 'The Utilities Page';
    }

    public function actionImportData()
    {
        $file = 'data/worker_health_final.csv';        
        $dates = [
            3 => '15th August 2016',
            4 => '19th September 2016',
            5 => '24th October 2016',
            6 => '26th September 2016',
            7 => '27th September 2016',
            8 => '29th August 2016',
            9 => '7th November 2016',
        ];
        $lines = file($file); //split into lines
        
        foreach($lines as $line){
            //split into array
            $items = explode(',', $line);
            //create new object and save
            
            $t = new \app\models\Trainee();
            $t->state_id = $items[1];
            $t->surname = $items[2];
            $t->firstname = $items[3];
            $t->phone = $items[4];
            $t->email = $items[5];
            $t->facility_id = $items[7];
            $t->lga_id = $items[9];
            $t->save(FALSE);

            $date_id = array_search($items[10], $dates);
            //save training details
            $tr = new \app\models\TraineeTraining();
            $tr->trainee_id = $t->id;
            $tr->training_id = $date_id;
            $tr->save(FALSE);           
            
            
        }
    }
    
    public function actionImportWards()
    {
        ini_set('max_execution_time',900);
        $file = 'data/wards_list_final.csv';
        
        $lines = file($file); //split into lines
        
        foreach($lines as $line){
            $items = explode(',', $line);
            
            $ward = new \app\models\Ward();
            $lga = \app\models\Lga::findOne($items[0]);
            $state_id = $lga->state_id;
            
            $ward->state_id = $state_id;
            $ward->lga_id = $items[0];
            $ward->name = $items[1];
            $ward->save(FALSE);
        }
    }
    
    public function actionRandomizeWards()
    {
        $ids = range(1, 433);
        for($i = 0; $i < count($ids); $i++){
            $trainee = \app\models\Trainee::findOne($ids[$i]);
            
            //retrieve all ward ids mapped to this trainee's lga
            $wards = \app\models\Ward::find()->where(['lga_id'=> $trainee->lga_id])->all();
            $ward_ids = [];
            foreach($wards as $ward){
                $ward_ids[] = $ward->id;
            }
            
            //select a random ward_id and assign to this trainee then save
            $ward_id = $ward_ids[mt_rand(0, count($ward_ids) - 1)];
            $trainee->ward_id = $ward_id;
            $trainee->save(FALSE);
        }        
    }
}
