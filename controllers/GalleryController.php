<?php

namespace app\controllers;

use Yii;
use app\models\Gallery;
use yii\filters\AccessControl;
use app\models\GallerySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\GalleryPhotoForm;
use yii\web\UploadedFile;
use yii\data\Pagination;


/**
 * GalleryController implements the CRUD actions for Gallery model.
 */
class GalleryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Gallery models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GallerySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Gallery model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $gallerypictures = \app\models\GalleryPicture::find()->where(['gallery_id'=> $id])->all();
        
        return $this->render('view', [
            'model' => $model,
            'gallerypictures' => $gallerypictures
        ]);
    }

    /**
     * Creates a new Gallery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Gallery();
        $upload = new GalleryPhotoForm();
        $upload->scenario = GalleryPhotoForm::SCENARIO_CREATE;
        
        if(Yii::$app->request->isPost){ //if form is submitted
            $upload->photo = UploadedFile::getInstance($upload, 'photo'); //get uploaded file instance
            if($upload->upload()){ //if file is uploaded successfully, save to DB
                if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                    $model->mainphoto = $upload->dbName;
                    $model->save();
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
             
        } else {
            return $this->render('create', [
                'model' => $model,
                'upload' => $upload,
            ]);
        }
    }

    /**
     * Updates an existing Gallery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        error_reporting(-1);
        ini_set('display_errors', true);

        $model = $this->findModel($id);
        $upload = new GalleryPhotoForm();
        $upload->scenario = GalleryPhotoForm::SCENARIO_UPDATE;
        
        $oldfile = $model->mainphoto;
        
        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post()) && $model->validate()){ //if form is submitted
            $upload->photo = UploadedFile::getInstance($upload, 'photo'); //get uploaded file instance
            if($upload->photo != NULL){//file exists
                $upload->upload();//upload new file
                unlink(Yii::$app->basePath.'/'.GalleryPhotoForm::GALLERY_FOLDER.$oldfile); //delete old file
                $model->mainphoto = $upload->dbName; //add uploaded file name to model
            }
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }else {
            return $this->render('update', [
                'model' => $model,
                'upload' => $upload,
            ]);
        }
    }

    /**
     * Deletes an existing Gallery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        unlink(Yii::$app->basePath.'/'.GalleryPhotoForm::GALLERY_FOLDER.$model->mainphoto); //delete old file
        $model->delete();
        
        return $this->redirect(['index']);
    }

    /**
     * Finds the Gallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Gallery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Gallery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionPagedDisplay()
    {
        $query = Gallery::find();
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count]);
        
        // limit the query using the pagination and retrieve the articles
        $galleries = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        
        return $this->render('paged-display',[
           'galleries' => $galleries,
            'pagination' => $pagination
        ]);
    }
    
    public function actionContentDisplay($id)
    {
        $pictures = \app\models\GalleryPicture::find()->where(['gallery_id'=> $id])->all();
        if(count($pictures) < 1) //if there are no gallery pictures to display
        {
            throw new \yii\web\HttpException(404, "There are no additional pictures in this gallery");
        }
        $gallery = Gallery::findOne($id);      
        $gallery_title = $gallery->title;
        return $this->render('content-display',[
            'pictures' => $pictures,
            'gallery' => $gallery
        ]);
    }
}
