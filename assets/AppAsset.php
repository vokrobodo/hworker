<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [        
        'web/css/semantic.min.css',
        'web/css/sidebar.min.css',
        'web/css/dropdown.min.css',
        'web/css/site.css',
        'web/css/leaflet.css'
    ];
    public $js = [
        //'web/js/highcharts.js',
        'web/js/semantic.min.js',
        'web/js/sidebar.min.js',
        'web/js/dropdown.min.js',
        //'web/js/highcharts.js',
        'web/js/mycharts.js',
        'web/js/leaflet.js',
        'web/js/maplet.js',
        'web/js/cleave.js',
        'web/js/liquidmetal.js',
        'web/js/jquery.flexselect.js'
    ];
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
