<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\ResourceCategory;
use app\models\ResourceType;

/* @var $this yii\web\View */
/* @var $model app\models\Resource */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="resource-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'class'=>'ui form'],
        
        ]); ?>
    
    <?= $form->errorSummary([$model, $upload]) ?>

    <?= $form->field($model, 'category_id')->dropDownList(ResourceCategory::getCategoryOptions(), ['propmpt' => '']) ?>

    <?= $form->field($model, 'type_id')->dropDownList(ResourceType::getTypeOptions(), ['propmpt' => '']) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($upload, 'file')->fileInput() ?>

    <?= $form->field($model, 'description')->textarea() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
