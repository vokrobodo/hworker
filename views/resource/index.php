<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\ResourceCategory;
use app\models\ResourceType;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ResourceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Resources';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="resource-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Resource', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'category_id',
                'value' => function($model){
                    return $model->category->name;
                },
                'filter' => ResourceCategory::getCategoryOptions()
            ],
            [
                'attribute' => 'type_id',
                'value' => function($model){
                    return $model->type->name;
                },
                'filter' => ResourceType::getTypeOptions()
            ],
            'name',
            //'filename',
            // 'description',
            // 'create_time',
            // 'create_user',
            // 'update_time',
            // 'update_user',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
