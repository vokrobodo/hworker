<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Resource */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Resources', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="resource-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            [
                'attribute' => 'category_id',
                'value' => $model->category->name
            ],
            [
                'attribute' => 'type_id',
                'value' => $model->type->name
            ],
            'name',
//            'filename',
            'description',
            [
                'attribute' => 'fileName',
                'value' => Html::a('View File', Url::base().'/resource-uploads/'.$model->filename, ['class'=>'btn btn-default', 'target'=>'_blank']),
                'format' => 'html'
            ],
//            'create_time',
//            'create_user',
//            'update_time',
//            'update_user',
        ],
    ]) ?>

</div>
