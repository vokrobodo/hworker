<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class='row'>
    <?php if(empty($resources)): ?>
    <div class="alert alert-info">There are no resources for this category</div>
    <?php else: ?>
    <br/>
    <h2><?= ucwords($category_name) ?> Resources</h2>
    <table class='table ui'>
        <tr>
            <th>SN</th>
            <th>File</th>
            <th>Type</th>
            <th>Action</th>
        </tr>
        <?php $sn = 1;?>
        <?php foreach($resources as $res): ?>
        <tr>
            <td><?= $sn ?></td>
            <td><?= $res->name ?></td>
            <td><?= $res->type->name ?></td>
            <td><?= Html::a('View File', Url::base().'/resource-uploads/'.$res->filename, ['class'=>'btn btn-default', 'target'=>'_blank']) ?></td>
        </tr>
        <?php $sn++; ?>
        <?php endforeach; ?>
    </table>
    <?php endif; ?>
</div>
