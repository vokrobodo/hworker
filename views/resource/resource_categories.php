<?php
use yii\helpers\Html;
?>
<div class="row">
    <?php if(empty($categories)): ?>
    <div class='alert alert-info'>No categories have been created</div>
    <?php else: ?>
    <div class="col-md-8">
        <h2>Resource Categories</h2>
        <ul>
        <?php foreach($categories as $cat): ?>
            <li><?= Html::a($cat->name, ['view-category-resources','id' => $cat->id]) ?></li>
        <?php endforeach; ?>
        </ul>    
    </div>
    <?php endif; ?>
</div>
