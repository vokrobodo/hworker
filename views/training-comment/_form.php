<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TrainingComment */
/* @var $form yii\widgets\ActiveForm */
?>
<script src="https://cdn.ckeditor.com/ckeditor5/11.2.0/classic/ckeditor.js"></script>
<style>
    .ck-editor__editable{
        height: 400px;
    }
</style>

<div class="training-comment-form col-md-10">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model) ?>

    <?= $form->field($model, 'training_id')->dropDownList([$training_id => $training_title]) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 12, 'id' => 'editor']) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['id' => 'submit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    let editor;
    ClassicEditor
        .create( document.querySelector( '#editor' ) )
        .then( newEditor => {
            editor = newEditor;
        } )
        .catch( error => {
            console.error( error );
        } );
</script>
<script>
    /*
    document.querySelector( '#submit' ).addEventListener( 'click', (evt) => {
        const editorData = $.trim(editor.getData());
        alert(editorData);
        evt.preventDefault();
    } );
    */
</script>
