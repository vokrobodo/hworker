<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TrainingComment */

$this->title = 'Create Training Comment';
$this->params['breadcrumbs'][] = ['label' => 'Training Comments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="training-comment-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'training_id' => $training_id,
        'training_title' => $training_title,
    ]) ?>

</div>
