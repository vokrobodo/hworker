<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="alert alert-success">
            Thank you for contacting us. We will respond to you as soon as possible.
        </div>

        <p>
            Note that if you turn on the Yii debugger, you should be able
            to view the mail message on the mail panel of the debugger.
            <?php if (Yii::$app->mailer->useFileTransport): ?>
                Because the application is in development mode, the email is not sent but saved as
                a file under <code><?= Yii::getAlias(Yii::$app->mailer->fileTransportPath) ?></code>.
                Please configure the <code>useFileTransport</code> property of the <code>mail</code>
                application component to be false to enable email sending.
            <?php endif; ?>
        </p>

    <?php else: ?>
        <div class="row">
            <div class="col-md-6 ui message">
                If you have business inquiries or other questions, please fill out the following form to contact us.
                Thank you.
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="ui middle aligned center aligned grid">
                    <div class="column">
                      <h2 class="ui teal image header">
                        <!--<img src="assets/images/logo.png" class="image">-->
                        <div class="content">
                          
                        </div>
                      </h2>
                      <?php $form = ActiveForm::begin(['id' => 'contact-form', 'class'=>'ui large form']); ?>
                        <div class="ui stacked segment">
                            <div class="fields"> 
                          <div class="field">
                            <div class="ui left icon input">
                              <i class="user icon"></i>
                              <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>
                            </div>
                          </div>
                          <div class="field">
                            <div class="ui left icon input">
                              <i class="lock icon"></i>
                              <?= $form->field($model, 'email') ?>
                            </div>
                          </div>
                         <div class="field">
                            <div class="ui left icon input">
                              <i class="lock icon"></i>
                              <?= $form->field($model, 'subject') ?>
                            </div>
                          </div>
                          <div class="field">
                            <div class="ui left icon input">
                              <i class="lock icon"></i>
                              <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>
                            </div>
                          </div>
                          <div class="field">
                            <div class="ui left icon input">
                              <i class="lock icon"></i>
                              <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                                //'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                            ]) ?>
                            </div>
                          </div>
                            <?= Html::submitButton('Submit', ['class' => 'ui fluid large teal submit button', 'name' => 'contact-button']) ?>
                          <!--<div class="ui fluid large teal submit button">Submit</div>-->
                        </div>
                        </div>
                        <div class="ui error message"></div>

                      <?php ActiveForm::end(); ?>
                      
                </div>
            </div>

            </div>
            <div class="col-md-6">
                
            </div>
        </div>

        

    <?php endif; ?>
</div>
