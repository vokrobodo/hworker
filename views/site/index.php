<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
$this->title = 'NPHCDA Health Workers Database';


?>
<style>
    .partner-logo-block img{
        width: 120px;
        height: 100px;
    }
    .partner-logo-block{
        position: relative;
        top:220px;
        text-align: center;
    },
    .partner-logo-block  img.chai-logo{
        width: 200px;
    }

</style>
<br/><br/>

<div class="row" id="index-page">
    <div class="col-md-1">
        <img src="web/img/bg_22.jpg" class="bg"/>
    </div>
    <div class="col-md-6 welcome-text">National Training Information Portal</div>
    <div class="col-md-5">
        <div class="ui middle aligned center aligned grid">
        <div class="column">
          <h2 class="ui image header">            
            <div class="content color-white">
              Log-in to your account
            </div>
          </h2>
          <?php $form = ActiveForm::begin(['options' => ['class'=>'ui large form']]); ?>
            
            <?= $form->errorSummary($model) ?>
          <!--<form method='post' class="ui large form">-->
            <div class="ui stacked segment">
              <div class="field">
                <div class="ui left icon input">
                  <i class="user icon"></i>
                  <!--<input type="text" name="email" placeholder="E-mail address">-->
                  <!--<input type="text" id="loginform-username" class="form-control" name="LoginForm[username]" autofocus aria-required="true" placeholder="Username">-->
                  <?php // $form->field($model, 'username')->textInput(['autofocus' => true, 'class'=>'ui col-md-12'])->label(FALSE) ?>
                  <?= Html::activeTextInput($model, 'username',['placeholder'=>'Username']) ?>
                </div>
              </div>
              <div class="field">
                <div class="ui left icon input">
                  <i class="lock icon"></i>
                  <!--<input type="password" name="password" placeholder="Password">-->
                  <!--<input type="password" id="user-password" class="form-control" name="User[password]" value="pass" maxlength="255" aria-required="true" aria-invalid="true" placeholder="Password">-->
                  <?php // $form->field($model, 'password')->passwordInput(['class'=>'ui col-md-6'])->label(FALSE) ?>
                  <?= Html::activePasswordInput($model, 'password',['placeholder' => 'Password']) ?>
                </div>
              </div>
              <!--<div class="ui fluid large teal submit button">Login</div>-->
              <button type="submit" class="ui fluid large teal submit button">Log in</button>
            </div>

            <div class="ui error message"></div>
            <?php ActiveForm::end(); ?>
          <!--</form>-->

          <!--<div class="ui message">
            New to us? <a href="#">Sign Up</a>
          </div>-->
        </div>
      </div>
    </div>

    <div class="col-md-12 partner-logo-block">
        <?= Html::img(Url::base().'/web/img/BMGF.png') ?>
        <?= Html::img(Url::base().'/web/img/CHAI.png', ['class' => 'chai-logo']) ?>
        <?= Html::img(Url::base().'/web/img/ivac.jpg') ?>
        <?= Html::img(Url::base().'/web/img/MCSP.jpeg') ?>
        <?= Html::img(Url::base().'/web/img/NPHCDA.png') ?>
        <?= Html::img(Url::base().'/web/img/UNICEF-LOGO-1.jpg') ?>
        <?= Html::img(Url::base().'/web/img/WHO-Logo.jpg') ?>
    </div>
    
    
</div>