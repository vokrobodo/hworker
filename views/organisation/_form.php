<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use app\models\Organisation;
use app\models\State;
use app\models\Lga;

/* @var $this yii\web\View */
/* @var $model app\models\Organisation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="organisation-form">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-6\">{input}</div>\n\n<div class=\"col-lg-2\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-2 control-label'],
        ],
    ]); ?>
    
    <?= $form->errorSummary([$model]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textArea(['rows' => 6]) ?>
    
    <?= $form->field($model, 'contact')->textInput() ?>
    
    <?= $form->field($model, 'website')->textInput(['placeholder'=>'web address must start with http://']) ?>
    
    <?= $form->field($model, 'specialization')->textInput() ?>

   <?= $form->field($model, 'state_id')->dropDownList(State::getStateOptions()) ?>

    <?= $form->field($model, 'lga_id')->dropDownList(Lga::getLgaOptions()) ?>

    <?= $form->field($model, 'type')->dropDownList(Organisation::getTypeOptions()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
