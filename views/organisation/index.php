<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrganisationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Organisations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="organisation-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Organisation', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute'=>'name',
                'value'=> function($model){
                    return strtoupper($model->name);
                }
            ],
            'address:ntext',
            [
                'attribute'=>'name',
                'value'=> function($model){
                    return ($model->state->name);
                },
                'filter'=> \app\models\State::getStateOptions()
            ],
            [
                'attribute'=>'type',
                'value'=> function($model){
                    return app\models\Organisation::getTypeText($model->type);
                },
                'filter'=> app\models\Organisation::getTypeOptions()
            ],
            //'lga_id',
            // 'status',
            // 'create_time',
            // 'create_user',
            // 'update_time',
            // 'update_user',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
