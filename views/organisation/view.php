<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Organisation;
/* @var $this yii\web\View */
/* @var $model app\models\Organisation */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Organisations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="organisation-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if(Yii::$app->session->hasFlash('success_message')): ?>
    <div class="alert alert-info">
         <?= Yii::$app->session->getFlash('success_message') ?>
    </div>
    <?php endif; ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            [
                'attribute'=>'name',
                'value'=> strtoupper($model->name)
            ],
            'address:ntext',
            'contact',
            'website',
            'specialization',
            [
                'attribute'=>'state_id',
                'value'=> $model->state->name
            ],
            [
                'attribute'=>'lga_id',
                'value'=> $model->lga->name
            ],
            [
                'attribute'=>'type',
                'value'=> Organisation::getTypeText($model->type)
            ],
//            [
//                'attribute'=>'status',
//                'value'=> Organisation::getStatusText($model->status)
//            ],
//            'create_time',
//            'create_user',
//            'update_time',
//            'update_user',
        ],
    ]) ?>

</div>
