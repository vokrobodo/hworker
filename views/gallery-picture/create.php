<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\GalleryPicture */

$this->title = 'Create Gallery Picture';
$this->params['breadcrumbs'][] = ['label' => 'Gallery Pictures', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gallery-picture-create">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?= $this->render('_form', [
        'model' => $model,
        'upload' => $upload,
        'gallery_id' => $gallery_id
    ]) ?>

</div>
