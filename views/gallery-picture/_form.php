<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Gallery;

/* @var $this yii\web\View */
/* @var $model app\models\GalleryPicture */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gallery-picture-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'gallery_id')->dropDownList([$gallery_id => Gallery::getTitle($gallery_id)]) ?>

    <?= $form->field($upload, 'photo')->fileInput() ?><span class="file-upload-instructions">file must not be greater than 1MB and must be jpeg or png</span>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
