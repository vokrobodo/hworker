<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Trainee;
use app\models\Training;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TraineeTrainingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Trainee Trainings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trainee-training-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Trainee Training', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'trainee_id',
            //'training_id',
            [
                'attribute' => 'facility_id',
                'value' => function($model){
                    return \app\models\Healthfacility::findOne($model->facility_id)->name;
                },
                'filter' => \app\models\Healthfacility::getFacilityOptions()
            ],
            [
                'attribute' => 'trainee_id',
                'value' => function($model){
                    return ucfirst(strtolower(Trainee::getFullName($model->trainee_id)));
                }
            ],
            [
                'attribute' => 'training_id',
                'value' => function($model){
                    return Training::getTitle($model->training_id);
                },
                'filter' => Training::getTrainingOptions()
            ],
            'score',
            [
                 'attribute' => 'rating',
                 'value' => function($model){
                    return Training::getRatingText($model->rating);
                 },
                 'filter' => Training::getRatingOptions()
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
