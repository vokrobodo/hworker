<?php

use yii\helpers\Html;
use app\models\Trainee;
use app\models\Training;

/* @var $this yii\web\View */
/* @var $model app\models\TraineeTraining */

$this->title = 'Update Trainee Training: ' .Trainee::getFullName($model->trainee_id)." - ".Training::getTitle($model->training_id);
$this->params['breadcrumbs'][] = ['label' => 'Trainee Trainings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="trainee-training-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
