<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TraineeTraining */

$this->title = 'Create Trainee Training';
$this->params['breadcrumbs'][] = ['label' => 'Trainee Trainings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trainee-training-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
