<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Trainee;
use app\models\Training;

/* @var $this yii\web\View */
/* @var $model app\models\TraineeTraining */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trainee-training-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'trainee_id')->dropDownList([$trainee->id => Trainee::getFullName($trainee->id)]) ?>

    <?= $form->field($model, 'training_id')->dropDownList(Training::getTrainingOptions()) ?>

    <?= $form->field($model, 'score')->textInput() ?>

    <?= $form->field($model, 'rating')->dropDownList(Training::getRatingOptions()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
