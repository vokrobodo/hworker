<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Trainee;
use app\models\Training;

/* @var $this yii\web\View */
/* @var $model app\models\TraineeTraining */

$this->title = Training::getTitle($model->training_id);
//$this->params['breadcrumbs'][] = ['label' => 'Trainee Trainings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Trainee::getFullName($model->trainee_id), 'url' => ['/trainee/view', 'id' =>$model->trainee_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trainee-training-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            [
                'attribute' => 'trainee_id',
                'value' => function($model){
                    return Trainee::getFullName($model->trainee_id);
                }
            ],
            [
                'attribute' => 'training_id',
                'value' => function($model){
                    return Training::getTitle($model->training_id);
                },
                'filter' => Training::getTrainingOptions()
            ],
            'score',
            [
                'attribute' => 'rating',
                'filter' => Training::getRatingOptions(),
                'value' => Training::getRatingText($model->rating)
            ],
        ],
    ]) ?>

</div>
