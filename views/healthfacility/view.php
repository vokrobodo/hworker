<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Healthfacility;
use app\models\Trainee;

/* @var $this yii\web\View */
/* @var $model app\models\Healthfacility */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Healthfacilities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="healthfacility-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if(Yii::$app->session->hasFlash('success_message')): ?>
    <div class="alert alert-info">
         <?= Yii::$app->session->getFlash('success_message') ?>
    </div>
    <?php endif; ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            [
                'attribute'=>'state_id',
                'value'=> empty($model->state_id)? "" : $model->state->name
            ],
            [
                'attribute'=>'lga_id',
                'value'=> empty($model->lga_id)? "" : $model->lga->name
            ],
            //'ward_id',
            [
                'attribute'=>'name',
                'value'=> strtoupper($model->name)
            ],
            'address:ntext',
            [
                'attribute'=>'status',
                'value'=> empty($model->status)? "" : Healthfacility::getStatusText($model->status)
            ],
            'latitude',
            'longitide',
//            'create_time',
//            'create_user',
//            'update_time',
//            'update_user',
        ],
    ]) ?>

</div>

<div class="row">
    <div class="col-md-12">
        <h3>TRAINING STATISTICS</h3>
    </div>
</div>

<?php if(count($training_counts > 0)): ?>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-bordered">
            <tr>
                <th>Trainings Conducted</th>
                <th>Conducted By</th>
                <th>Venue</th>
                <th>Date</th>
                <th>Count</th>
                <th>Trainee List</th>
            </tr>
            <?php foreach ($training_counts as $training): ?>
                <?php //$training = Training::findOne($id) ?>
                <tr>
                    <td><?= $training['title'] ?></td>
                    <td><?= \app\models\Organisation::getName($training['organizer']) ?></td>
                    <td><?= ucfirst(strtolower($training['venue'])) ?></td>
                    <td><?= Yii::$app->formatter->asDate($training['startdate'])." - ".Yii::$app->formatter->asDate($training['enddate']) ?></td>
                    <td><?= Trainee::getTrainingCountByFacility($model->id, $training['id']) ?></td>
                    <td><?= Html::a('View Trainee List', ['trainee-list', 'id'=> $model->id, 'training_id' => $training['id']], ['class' => 'btn btn-primary']) ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>
<?php else: ?>
    <div class="row">
        <div class="col-md-12">
            <h3>There are no training statistics for this health facility</h3>
        </div>
    </div>
<?php endif; ?>
