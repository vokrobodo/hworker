<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Healthfacility;
use app\models\Training;

/* @var $this yii\web\View */
/* @var $model app\models\Healthfacility */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Healthfacilities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="healthfacility-view">
    <table class="table table-bordered table-striped">
        <tr>
            <th>Trainee List</th>
        </tr>
        <?php foreach ($trainees as $trainee): ?>
        <tr>
            <td><?= ucfirst(strtolower($trainee['surname']))." ".ucfirst(strtolower($trainee['firstname'])) ?></td>
        </tr>
        <?php endforeach; ?>
    </table>
</div>

