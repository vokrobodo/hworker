<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Healthfacility */

$this->title = 'Create Health facility';
$this->params['breadcrumbs'][] = ['label' => 'Healthfacilities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="healthfacility-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
