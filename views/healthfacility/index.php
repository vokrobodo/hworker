<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\State;
use app\models\Lga;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HealthfacilitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Health facilities';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="healthfacility-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Health facility', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            /*
            [
                'attribute'=>'state_id',
                'value' => function($model){
                    return empty($model->state_id)? "" : State::getStateName($model->state_id);
                }
            ],
            [
                'attribute'=>'lga_id',
                'value' => function($model){
                    return empty($model->lga_id)? "" : Lga::getLgaName($model->lga_id);
                }
            ],
            
            [
                'attribute'=>'ward_id',
                'value' => function($model){
                    return empty($model->ward_id)? "" : app\models\Ward::getWardName($model->ward_id);
                }
            ],*/
            [
                'attribute'=>'name',
                'value' => function($model){
                    return strtoupper($model->name);
                }
            ],
            // 'address:ntext',
            // 'status',
            // 'latitude',
            // 'longitide',
            // 'create_time',
            // 'create_user',
            // 'update_time',
            // 'update_user',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
