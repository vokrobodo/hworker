<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use app\models\Healthfacility;
use app\models\State;
use app\models\Lga;
use yii\helpers\Url;
use app\models\Ward;

/* @var $this yii\web\View */
/* @var $model app\models\Healthfacility */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="healthfacility-form">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-6\">{input}</div>\n\n<div class=\"col-lg-2\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-2 control-label'],
        ],
    ]); ?>
    
    <?= $form->errorSummary([$model]) ?>

    <?= $form->field($model, 'state_id')->dropDownList(State::getStateOptions(),['prompt' => '']) ?>

    <?= $form->field($model, 'lga_id')->dropDownList(Lga::getLgaOptions(), ['prompt' => '']) ?>

    <?= $form->field($model, 'ward_id')->dropDownList(Ward::getWardOptions(), ['prompt' => '']) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->dropDownList(Healthfacility::getStatusOptions()) ?>

    <?= $form->field($model, 'latitude')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'longitide')->textInput(['maxlength' => true]) ?>
    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$this->registerJsFile(
    Url::base().'/web/js/lga_select.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);