<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Trainee */

$this->title = 'Update Trainee: ' . $model->surname." ".$model->firstname;
$this->params['breadcrumbs'][] = ['label' => 'Trainees', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="trainee-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'training' => $training,
        'train' => $train,
//        'upload' => $upload
    ]) ?>

</div>
