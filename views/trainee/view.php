<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use app\libraries\Options;
use app\models\Training;
use app\models\Ward;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Trainee */

$this->title = $model->surname." ".$model->firstname;
$this->params['breadcrumbs'][] = ['label' => 'Trainees', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trainee-view">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?php if(Yii::$app->session->hasFlash('success_message')): ?>
    <div class="alert alert-info">
         <?= Yii::$app->session->getFlash('success_message') ?>
    </div>
    <?php endif; ?>
    
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <div class="pull-left">
        <?php if($model->photo): ?>
            <?= Html::img(Url::base().'/photo-uploads/'.$model->photo, ['class'=>'trainee_img']) ?>
        <?php else: ?>
            <?= Html::img(Url::base().'/web/img/person1.jpg', ['class'=>'trainee_placeholder_img']) ?>
        <?php endif; ?>
    </div>
    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'ui clled table striped'],
        'attributes' => [
            //'id',
            'surname',
            'firstname',
            'middlename',
            [
                'attribute'=>'phone',
                'format'=>'html',
                'value'=>function($model){
                    $string = str_replace(' ', '', $model->phone);
                    return "<span class='phone'>$string</span>";
                }
            ],
            'email:email',
            [
                'attribute'=>'gender',
                'value'=> Options::getGenderText($model->gender)
            ],
            [
                'attribute'=>'state_id',
                'value'=> $model->state->name
            ],
            [
                'attribute'=>'lga_id',
                'value'=> $model->lga->name
            ],
            [
                'attribute'=>'ward_id',
                'value'=> ucwords(strtolower(Ward::getWardName($model->ward_id)))
            ],
            [
                'attribute'=>'facility_id',
                'value'=> isset($model->facility_id)? $model->healthfacility->name : ''
            ],
            'serviceyears',
            'position',
            'comment'
            //'photo',
        ],
    ]) ?>
    

</div>
    <hr/>

<div class="row">
    <div class="col-md-2"><b>Trainings Attended</b></div>
    <div class="col-md-2 col-md-offset-8"><?= Html::a('Add New Training',['/trainee-training/add', 'id' => $model->id],['class' =>'btn btn-success']) ?></div>
</div>
<br>
<div class="row">
 <div class="col-md-12">


     <?php if(empty($training)):?>
         <div class="alert alert-info">No trainings available for this worker</div>
     <?php else: ?>
         <table class="ui table">
             <tr>
                 <th>Training</th>
                 <th>Description</th>
                 <th>Date</th>
                 <th>Score</th>
                 <th>Rating</th>
                 <th>Action</th>
             </tr>
             <?php foreach($training as $t): ?>
                 <tr>
                     <td><?= Training::getTitle($t->training_id) ?></td>
                     <td><?= Training::getDescription($t->training_id)?></td>
                     <td><?= Training::getDates($t->training_id)?></td>
                     <th><?= $t->score ?></th>
                     <td><?= Training::getRatingText($t->rating) ?></td>
                     <td><?= Html::a('Update', ['/trainee-training/update', 'id' => $t->id], ['class'=>'btn btn-default']) ?></td>
                 </tr>
             <?php endforeach; ?>
         </table>
     <?php endif; ?>
 </div>
</div>

<?php
$this->registerJsFile(
    Url::base().'/web/js/number-format.js',
    ['position' => View::POS_END]
);