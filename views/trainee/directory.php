<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\View;
use yii\helpers\Url;
use app\models\Ward;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TrainerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Trainees';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trainer-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Trainee', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?=  GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute'=> 'surname',
                'value' => function($model){
                    return ucfirst(strtolower($model->surname));
                }
            ],
            [
                'attribute'=> 'firstname',
                'value' => function($model){
                    return ucfirst(strtolower($model->firstname));
                }
            ],
            //'middlename',
            [
                'attribute'=> 'ward_id',
                'value' => function($model){
                    return str_replace('?','',ucfirst(strtolower(Ward::getWardName($model->ward_id))));
                }
            ],
//            [
//                'attribute' => 'phone',
//                'value' => function($model){
//                    if(substr($model->phone, 0, 1) != 0){
//                        return "0".$model->phone;
//                    }
//                    return $model->phone;
//                }
//            ],
            [
                'attribute'=>'phone',
                'format'=>'html',
                'value'=>function($model){
                    $string = str_replace(' ', '', $model->phone);
                    return "<span class='phone'>$string</span>";
                }
            ],
            [
                'attribute'=>'state_id',
                'value' => function($model){
                    return \app\models\State::getStateName($model->state_id);
                },
                'filter' => \app\models\State::getStateOptions(),
            ],
            // 'email:email',
            // 'gender',
            // 'state_id',
            // 'lga_id',
            // 'facility_id',
            // 'serviceyears',
            // 'position',
            // 'photo',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

<?php
$this->registerJsFile(
    Url::base().'/web/js/number-format.js',
    ['position' => View::POS_END]
);
