<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\web\View;
use app\models\Ward;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TraineeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Trainees';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trainee-index" id="app">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Trainee', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('View Directory', ['directory'], ['class' => 'btn btn-primary pull-right']) ?>
    </p>
    <hr/>
    <div class="row">
        <form class="form-inline">
            <div class="form-group col-md-4">
                <select name="state" id="select_state" class="form-control" v-on:change="getStateLgas">
                    <option value="" disabled selected>select state</option>
                    <option v-for="state in states" :key="state.id" v-bind:value="state.id">{{state.name}}</option>
                </select>
            </div>
            <div class="form-group col-md-4">
                <select name="lga" id="select_lga" class="form-control" v-on:change="getLgaWards">
                    <option value="" disabled selected>select lga</option>
                    <option v-for="lga in lgas" :key="lga.id" v-bind:value="lga.id">{{lga.name}}</option>
                </select>
            </div>
            <div class="form-group col-md-4">
                <select name="ward" id="select_ward" class="form-control" v-on:change="getWardFacilities">
                    <option value="" disabled selected>select ward</option>
                    <option v-for="ward in wards" :key="ward.id" v-bind:value="ward.id">{{ward.name}}</option>
                </select>
            </div>
        </form>
    </div>
    <hr>
    <div>
        <h1>{{stateName}}</h1>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="ui statistics">
                <div class="statistic">
                  <div class="value">
                    {{statesTrainedCount}}
                  </div>
                  <div class="label">
                    STATES
                  </div>
                </div>
                <div class="statistic">
                  <div class="value">
                  {{lgasTrainedCount}}
                  </div>
                  <div class="label">
                    LGAs
                  </div>
                </div>
                <div class="statistic">
                  <div class="value">
                  {{wardsTrainedCount}}
                  </div>
                  <div class="label">
                    Wards
                  </div>
                </div>
                <div class="statistic">
                  <div class="value">
                  {{totalTrainedCount}}
                  </div>
                  <div class="label">
                    Trained Health Workers
                  </div>
                </div>
            </div>
            <hr/>
            
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <p id="homechart"></p>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">State Distribution</div>
                <div class="panel-body" id="map" style="height: 360px;">

                </div>
            </div>
        </div>
    </div>
    <hr/>

    <div class="row">
        <div class="col-md-12">
            <h2>TRAINING DISTRIBUTION</h2>
        </div>
        <form action="" class="form-inline">
            <div class="form-group col-md-6">
                <select name="" id="" class="col-md-6 form-control" v-on:change="getTrainedList">
                    <option value="" disabled selected>select training</option>
                    <option v-for="training in trainingList" :key="training.id" v-bind:value="training.id">{{training.title}}</option>
                </select>
            </div>
        </form>
    </div>

    <hr/>
    <div class="row" v-if="countryTrainingStats.length > 0">
        <div class="col-md-12">
            <p id="training-chart" style="height: 400px; font-size: 50px"></p>
        </div>
    </div>
    
</div>




<script>
    var cdata = JSON.parse('<?= $state_distribution;?>');
    console.log(cdata)
    var state_names = JSON.parse('<?= $state_names;?>');

</script>

<?php
$this->registerJsFile(
    Url::base().'/web/js/highcharts.js',
    ['position' => View::POS_HEAD]
);

/*$this->registerJs(
    "$(document).ready(function () {
    // Build the chart
    Highcharts.chart('homechart', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'State distribution of health workers'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y}</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Number',
            colorByPoint: true,
            data: cdata
        }]
    });



});",
    View::POS_READY,
    'my-button-handler'
);

*/

?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){

        var states = state_names;
        var key =  'AIzaSyBE5SaJX5exxAy3VeZX2E3uhL6hm68p4qg';

        //initialize map
        var mymap = L.map('map').setView([9.0546462, 7.2542664], 6.4);

        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox.streets',
            accessToken: 'pk.eyJ1IjoiZG9udmlra3kiLCJhIjoiY2pkYW9tZWNuMnA3ZDMzbGIyMDBzeHQ2YyJ9.vA3m74OsLCIyzrEKJ6JFVg'
        }).addTo(mymap);

        //iterate through list and get coordinates from google's api
        for (var i=0; i< states.length; i++){

            url = 'https://maps.googleapis.com/maps/api/geocode/json?address='+states[i]+'&key='+key
            $.ajax({
                url: url,
                async: false,
                success: function(data){
                    var lat = data.results[0].geometry.location.lat;
                    var lng = data.results[0].geometry.location.lng;

                    setMapParams(lat,lng,states[i]);
                },
             });
            /*
            $.get(url,{'states':states},function( data ) {
                var lat = data.results[0].geometry.location.lat;
                var lng = data.results[0].geometry.location.lng;

                setMapParams(lat,lng,states[i])
            })
            */
        }

        function setMapParams(lat,lng,state){
            //add circles to map
            var circle = L.circle([lat, lng], {
                color: 'red',
                fillColor: '#f03',
                fillOpacity: 0.5,
                radius: 50000
            }).addTo(mymap);

            //add marker to map
            //console.log(states[i]);
            L.marker([lat, lng])
                    .bindPopup(state).openPopup().addTo(mymap);
        }

    })
</script>



<?php
$this->registerJsFile(
    Url::base().'/web/js/number-format.js',
    ['position' => View::POS_END]
);
?>
<?= Html::jsFile(Url::base().'/web/js/trainee.js') ?>

