<?php

use yii\helpers\Html;
use app\models\Healthfacility;
use app\models\TraineeTraining;
use yii\helpers\Url;
use yii\web\View;
?>

<?php if(empty($trainees)): ?>
<div class="alert alert-info">There are no trainees for the selected state</div>
<?php else: ?>
<!---------------------------------- show a card view of trainees --------------------------------------------->
<div class="row">
    <div class="col-md-6">
        <div class="ui statistic">
            <div class="value">
              <?= $lga_count ?>
            </div>
            <div class="label">
              LGAs
            </div>
        </div>
        <div class="ui statistic">
            <div class="value">
              <?= $ward_count ?>
            </div>
            <div class="label">
              Wards
            </div>
        </div>
        <div class="ui statistic">
            <div class="value">
              <?= count($trainees)?>
            </div>
            <div class="label">
              Health Workers
            </div>
        </div>
        <hr/>
        <p id="homechart"></p>
    </div>
    <div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">Map Distribution</div>
            <div class="panel-body">
                <?php if($state_id != 37): ?>
                    <h1>MAP NOT AVAILABLE</h1>
                <?php else: ?>
                    <p id="mapid"></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<hr/>

<div class="row">
    <?php foreach($trainees as $t): ?>
    <div class="col-md-3" id="state-trainee-report">
        <div class="ui card">
            <div class="image">
                <?php if(isset($t->photo)): ?>
                    <?= Html::img(Url::base().'/photo-uploads/'.$t->photo) ?>
                <?php else: ?>
                    <?= Html::img(Url::base().'/web/img/person1.jpg',['class'=>'trainee-placeholder-img']) ?>
                <?php endif; ?>              
            </div>
            <div class="content">
              <a class="header"><?= ucwords(strtolower($t->surname. " ".$t->firstname))?></a>
              <div class="meta">
                <span class="date"><?= ucwords($t->position) ?></span>
              </div>
              <div class="description">
                <?= Healthfacility::getName($t->facility_id)?>
              </div>
            </div>
            <div class="extra content">
              <span class="right floated">
                <?= Html::a('View Profile',['/trainee/view', 'id' => $t->id]) ?>
              </span>
              <span>
                <i class="user icon"></i>
                <?= TraineeTraining::getTrainingCount($t->id) ?> Training(s)
              </span>
            </div>
        </div>
        <br/>
    </div>
    <?php endforeach; ?>
    </div>

	
<?php endif; ?>
<script>
    var state = "<?= app\models\State::getStateName($state_id) ?>";
    var distribution = JSON.parse('<?= $lga_distribution;?>');
</script>


<?php
$this->registerJsFile(
    Url::base().'/web/js/highcharts.js',
    ['position' => View::POS_HEAD]
);

$this->registerJs(
    "$(document).ready(function () {
    
    // Build the chart
    Highcharts.chart('homechart', {
    chart: {
        type: 'column'
    },
    title: {
        text: state +' State :Trainees per Local Government'
    },
    subtitle: {
        text: 'Source: National Primary Health Care Development Agency'
    },
    xAxis: {
        type: 'category',
        labels: {
            //rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Segoe ui light'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Workers'
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'Number of trainees: {point.y}</b>'
    },
    series: [{
        name: 'Population',
        data: distribution,
        
    }]
})
    
    
    
});",
    View::POS_READY,
    'my-button-handler'
);
