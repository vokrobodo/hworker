<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Trainee */

$this->title = 'Directory - LGA Search';
$this->params['breadcrumbs'][] = ['label' => 'Trainees', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="trainee-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if(isset($report)): ?>
    <?= $this->render('_lga_report_table', [
        'trainees' => $trainees,
        'ward_count' => $ward_count,
        'ward_distribution'=> $ward_distribution,
        'lga_id' => $lga_id
    ]) ?>
    <?php else: ?>
    <?= $this->render('_lga_report_form', [
        'model' => $model,
    ]) ?>
    <?php endif; ?>
    

</div>
