<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use app\libraries\Options;
use app\models\State;
use app\models\Lga;
use app\models\Training;
use app\models\Healthfacility;
use yii\helpers\Url;
use yii\web\View;
use app\models\Ward;
/* @var $this yii\web\View */
/* @var $model app\models\Trainee */
/* @var $form yii\widgets\ActiveForm */
?>

<?php if (!$model->isNewRecord && $model->photo): ?>
<div class="row">
    <div class="col-sm-3 pull-right"><?= Html::img('photo-uploads/'.$model->photo) ?></div><br/>
</div>
<?php endif; ?>

<?php
$this->registerJs(
    "$('#multi-select').dropdown();",
    View::POS_END
);
?>

<div class="trainee-form">
    
    
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'class'=>'ui form'],
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-6\">{input}</div>\n\n<div class=\"col-lg-2\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-2 control-label'],
        ],
        ]); ?>
    
    <?= $form->errorSummary([$model]) ?>

    <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'middlename')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true,'class'=>'form-control phone']) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'dateofbirth')->widget(\yii\jui\DatePicker::className(), [
    // if you are using bootstrap, the following line will set the correct style of the input field
        'dateFormat'=>'yyyy-MM-dd',
        'options' => ['class' => 'form-control','dateFormat'=>'yy-mm-dd'],
                                                    'clientOptions' => ['dateFormat' => 'yy-mm-dd','changeMonth'=>'true', 'changeYear'=>'true', 'yearRange'=>'1900:2018']
    // ... you can configure more DatePicker properties here
        ]) 
    ?>

    <?= $form->field($model, 'gender')->dropDownList(Options::getGenderOptions(), ['prompt'=>'', 'class'=>'ui fluid dropdown']) ?>

    <?= $form->field($model, 'state_id')->dropDownList(State::getStateOptions(), ['prompt'=>'', 'class'=>'trainee-state ui search dropdown']) ?>

    <?= $form->field($model, 'lga_id')->dropDownList(Lga::getLgaOptions(),['prompt'=>'', 'class'=>'trainee-lga form-control']) ?>
    
    <?= $form->field($model, 'ward_id')->dropDownList(Ward::getWardOptions(),['prompt'=>'', 'class'=>'trainee-ward form-control']) ?>

    <?= $form->field($model, 'facility_id')->dropDownList(Healthfacility::getFacilityOptions(), ['prompt'=>'', 'class'=>'trainee-facility form-control']) ?>
    
    <?php if($model->isNewRecord): ?>
        <?= $form->field($training, 'training_id[]')->dropDownList(Training::getTrainingOptions(),
                ['multiple'=>'', 'class'=>'ui multiple selection dropdown', 'id'=>"multi-select"]) ?>
    <?php else: ?>
    <div class="form-group field-trainer-position required">
        <label class="col-lg-2 control-label" for="trainer-position">Position</label>
        <div class="col-lg-6">
        <?= 
            Html::activeDropDownList($train,'training_id[]', Training::getTrainingOptions(),
                    ['class'=>'form-control', 'multiple'=>'multiple', 'label'=>'Trainings', 'prompt'=>'Please Select', 'options'=>$training])?> 
        </div>
    </div>
    <?php endif; ?>

    <?= $form->field($model, 'serviceyears')->textInput() ?>

    <?= $form->field($model, 'position')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'latitude')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'longitude')->textInput(['maxlength' => true]) ?>
    
    <?php /* $form->field($model, 'comment')->textarea()*/ ?>

    <?php /* $form->field($upload, 'photo')->fileInput() */ ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
var cleave = new Cleave('.phone', {
    blocks: [4, 3, 4],
    uppercase: true
});

//ajax calls
$(document).ready(function(){

    //get lgas on state change
    $('.trainee-state').change(function(){
        var state = $(this).val()
        $.post('/hworker/lga/get-state-lga-options',{'state':state}, function(data){
            $('.trainee-lga').empty().append(data)
        })
    })

    //get wards on lga change
    $('.trainee-lga').change(function(){
        var lga = $(this).val();
        $.post('/hworker/ward/get-lga-options',{'lga':lga}, function(data){
            $('.trainee-ward').empty().append(data);
        })
    })
})
</script>
<?php
/*
$this->registerJsFile(
    Url::base().'/web/js/lga_select.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
*/
