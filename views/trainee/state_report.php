<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Trainee */

$this->title = 'Directory - State Search';
$this->params['breadcrumbs'][] = ['label' => 'Trainees', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="trainee-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <hr/>
    <?php if(isset($report)): ?>
    <?= $this->render('_state_report_table', [
        'trainees' => $trainees,
        'lga_count' => $lga_count,
        'ward_count' => $ward_count,
        'state_id' => $state_id,
        'lga_distribution' => $lga_distribution
    ]) ?>
    <?php else: ?>
    <?= $this->render('_state_report_form', [
        'model' => $model,
    ]) ?>
    <?php endif; ?>
    

</div>
