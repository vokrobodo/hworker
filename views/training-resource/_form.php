<?php


use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Training;
use app\models\ResourceCategory;
use app\models\Resource;

/* @var $this yii\web\View */
/* @var $model app\models\TrainingResource */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="training-resource-form col-md-6">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'training_id')->dropDownList([Yii::$app->request->get('id')  => Training::getTitle(Yii::$app->request->get('id'))], ['prompt' => '']) ?>

    <?= $form->field($model, 'resource_category_id')->dropDownList(ResourceCategory::getCategoryOptions(), ['prompt' => '', 'class'=>'form-control category_id']) ?>

    <?= $form->field($model, 'resource_id')->dropDownList(Resource::getOptions(), ['prompt' => '', 'class' => 'form-control resource_id']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    $(document).ready(function(){
        $('.category_id').change(function(){
            $('.resource_id').empty();
            var catid = $(this).val();
            $.get('/hworker/resource/get-by-category',{catid: catid}, function(data){
                console.log(data);
                $('.resource_id').empty().append(data);
            })
        })
    })
</script>