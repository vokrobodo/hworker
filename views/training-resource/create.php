<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TrainingResource */

$this->title = 'Create Training Resource';
$this->params['breadcrumbs'][] = ['label' => 'Training Resources', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="training-resource-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
