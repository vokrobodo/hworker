<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ResourceCategory */

$this->title = 'Create Resource Category';
$this->params['breadcrumbs'][] = ['label' => 'Resource Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="resource-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
