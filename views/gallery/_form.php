<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Gallery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gallery-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'],
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-6\">{input}</div>\n\n<div class=\"col-lg-2\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-2 control-label'],
        ],
        ]); ?>
    
    <?= $form->errorSummary([$model, $upload]) ?>
    
    <?php if(Yii::$app->session->hasFlash('error')): ?>
        <?= Yii::$app->session->getFlash('error') ?>
    <?php endif; ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'shortdescription')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'longdescription')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'date')->widget(DatePicker::className(),['dateFormat'=>'php:Y-m-d'], ['clientOptions' => ['dateFormat' => 'yy-mm-dd']]) ?>

    <?= $form->field($upload, 'photo')->fileInput() ?><span class="file-upload-instructions">file must not be greater than 1MB and must be jpeg or png</span>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
