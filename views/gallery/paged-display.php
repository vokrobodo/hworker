<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;

$this->title = 'Galleries';
?>

<?php if(empty($galleries)): ?>
    <div class="alert alert-info">No Galleries have been created</div>
<?php else: ?>
    <?php foreach($galleries as $g): ?>
    <div class="row">
        <div class="col-sm-3">
            <?= Html::img(Url::base().'/gallery-uploads/'.$g->mainphoto, ['width'=>'150', 'height'=>'150', 'class'=>'pull-right']) ?>
        </div>
        <div class="col-sm-9">
            <h4><?= $g->title ?></h4>
            <h5><?= Yii::$app->formatter->asDate($g->date) ?></h5>
            <p><?= $g->longdescription?></p>
            <?= Html::a('View Gallery',['gallery/content-display', 'id'=> $g->id])?>
        </div>
    </div>
    <hr/>
    <?php endforeach; ?>
<?php endif;  ?>

<?php echo LinkPager::widget([
    'pagination' => $pagination,
]);
?>