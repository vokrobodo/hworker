<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = ucwords($gallery->title);
?>

<h2><?= ucwords($gallery->title) ?></h2>
<h4><?= Yii::$app->formatter->asDate($gallery->date) ?></h4>
<h6><?= ucwords($gallery->shortdescription)?></h6>

<div id="carousel-example-generic" class="carousel slide col-md-6 col-md-offset-3" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <?php for($i = 0; $i < count($pictures); $i++): ?>
          <?php if($i == 0): ?>
              <li data-target="#carousel-example-generic" data-slide-to="<?= $i ?>" class="active"></li>
          <?php else: ?>
              <li data-target="#carousel-example-generic" data-slide-to="<?= $i ?>"></li>
          <?php endif; ?>

        <?php endfor; ?>
    </ol>
    
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <?php for($i = 0; $i < count($pictures); $i++): ?>
            <?php if($i == 0): ?>
                <div class="item active">
                    <?= Html::img(Url::base().'/gallery-uploads/'.$pictures[$i]->picture) ?>
                    <div class="carousel-caption">
                      <?= $pictures[$i]->description ?>
                    </div>
                </div>
            <?php else: ?>
                <div class="item">
                    <?= Html::img(Url::base().'/gallery-uploads/'.$pictures[$i]->picture) ?>
                    <div class="carousel-caption">
                      <?= $pictures[$i]->description ?>
                    </div>
                </div>
            <?php endif; ?>
        <?php endfor; ?>
    </div>
    
    <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
    
</div>