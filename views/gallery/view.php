<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Gallery */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Galleries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gallery-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Add More Photos',['gallery-picture/create', 'id'=> $model->id], ['class'=>'btn btn-success']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'title',
            'shortdescription:ntext',
            'longdescription:ntext',
            [
                'attribute'=>'date',
                'value'=>Yii::$app->formatter->asDate($model->date)
            ],
            [
                'attribute'=>'mainphoto',
                'value'=> Html::img(Url::base().'/gallery-uploads/'.$model->mainphoto, ['width'=>200, 'height'=>200]),
                'format' => 'html'
            ],
//            'create_time',
//            'create_user',
//            'update_time',
//            'update_user',
        ],
    ]) ?>

</div>

<div class="row">
    <?php if(!empty($gallerypictures)): ?>
        <?php for($i = 0; $i < count($gallerypictures); $i++): ?>            
            <div class="col-md-3">
                <h5><?= $gallerypictures[$i]->description ?></h5>
                <?= Html::img(Url::base().'/gallery-uploads/'.$gallerypictures[$i]->picture,['class'=>'col-md-4']) ?><br/><br/>
                <?= Html::a('Delete Photo', ['gallery-picture/delete', 'id'=> $gallerypictures[$i]->id],
                        ['class'=>'btn btn-default', 
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this photo?',
                            'method' => 'post',
                        ]]) ?>
            </div>
        <?php endfor; ?>
    <?php endif; ?>
</div>