<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.1.0/dist/leaflet.css"
   integrity="sha512-wcw6ts8Anuw10Mzh9Ytw4pylW8+NAD4ch3lqm9lzAsTxg0GFeJgoAtxuCLREZSC5lUXdVyo/7yfsqFjQ4S+aKw=="
   crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.1.0/dist/leaflet.js"
   integrity="sha512-mNqn2Wg7tSToJhvHcqfzLMU6J4mkOImSPTxVZAdo+lcPlk+GhZmYgACEe0x35K7YzW1zJ7XyJV/TT1MrdXvMcA=="
   crossorigin=""></script>
   <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
   <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Html::img(Url::base().'/web/img/nph_logo.png', ['alt'=>Yii::$app->name, 'class'=>'logo']).''.Html::img(Url::base().'/web/img/nigeria.png', ['alt'=>Yii::$app->name, 'class'=>'nigeria_logo']),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            //['label' => 'Users', 'url' => ['/user/index'], 'visible'=> !Yii::$app->user->isGuest],
            //['label' => 'About', 'url' => ['/site/about']],
            [
                'label'=>'Training','visible'=>!Yii::$app->user->isGuest,
                'items' => [
                    ['label' => 'Trainee Directory', 'url' => ['/trainee']],
                    ['label' => 'Trainee State View', 'url' => ['/trainee/state-report-form']],
                    ['label' => 'Trainee LGA View', 'url' => ['/trainee/lga-report-form']],
                    ['label' => 'Trainers Directory', 'url' => ['/trainer']],
                    ['label' => 'Trainers State View', 'url' => ['/trainer/state-report-form']],
                    ['label' => 'Trainers LGA View', 'url' => ['/trainer/lga-report-form']],
                    ['label' => 'Trainings Conducted', 'url' => ['/training']],
                    ['label' => 'Training Plans', 'url' => ['/training-plan']],
                    ['label' => 'Training Ratings', 'url' => ['/trainee-training']],
                    ['label' => 'Users', 'url' => ['/user']]
                ]
            ],
            /*
            [
                'label' =>'Trainees','visible'=> !Yii::$app->user->isGuest,
                'items' => [
                    ['label' => 'Directory', 'url' => ['/trainee']],
                    ['label' => 'State View', 'url' => ['/trainee/state-report-form']],
                    ['label' => 'LGA View', 'url' => ['/trainee/lga-report-form']],
                    
                ]
            ],
            [
                'label' =>'Trainers', 'visible'=> !Yii::$app->user->isGuest,
                'items' => [
                    ['label' => 'Directory', 'url' => ['/trainer']],
                    ['label' => 'State View', 'url' => ['/trainer/state-report-form']],
                    ['label' => 'LGA View', 'url' => ['/trainer/lga-report-form']]
                ]
            ],
            //['label' => 'Training Plans', 'url' =>['/training'], 'visible'=> !Yii::$app->user->isGuest,],
            [
                'label' =>'Training', 'visible'=> !Yii::$app->user->isGuest,
                'items' => [
                    ['label' => 'Trainings Conducted', 'url' => ['/training']],
                    ['label' => 'Training Plans', 'url' => ['/training-plan']],
                ]
            ],
            */
            ['label' => 'Organisations', 'url' =>['/organisation'], 'visible'=> !Yii::$app->user->isGuest,],
            ['label' => 'Health Facilities', 'url' =>['/healthfacility'], 'visible'=> !Yii::$app->user->isGuest,],
            [
                'label' => 'Gallery', 'visible'=> !Yii::$app->user->isGuest,
                'items' => [
                    ['label' => 'View Galleries', 'url' => ['/gallery/paged-display']],
                    ['label' => 'New Gallery', 'url' => ['/gallery/index']]
                ]
            ],
            ['label' => 'Qualifications', 'url' =>['/qualification'], 'visible'=> !Yii::$app->user->isGuest,],
            [
                'label' => 'Resources', 'visible'=> !Yii::$app->user->isGuest,
                'items' => [
                    ['label' => 'View Resources', 'url' => ['/resource/view-categories']],
                    ['label' => 'Add New Resource', 'url' => ['/resource/index']],
                    ['label' => 'Resource Categories', 'url' => ['/resource-category/index']],
                    ['label' => 'Resource Types', 'url' => ['/resource-type/index']],
                ]
            ],
            ['label' => 'Contact', 'url' => ['/site/contact']],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&COPY; NPHCDA <?= date('Y') ?></p>

        <p class="pull-right">Powered by <a href="#"><?= Html::img('web/img/delwu.png',['class'=>'dcl_logo']) ?></a></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
