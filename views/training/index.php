<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\State;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TrainingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Conducted Trainings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="training-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Add New Conducted Training', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'training_id',
            'title',
            //'description:ntext',
            [
                'attribute' => 'organizer',
                'value' => function($model){
                    return \app\models\Organisation::getName($model->organizer);
                }
            ],
            'venue:ntext',
            [
                'attribute'=>'state_id',
                'value' => function($model){
                    return State::getStateName($model->state_id);
                },
                'filter' => ArrayHelper::map(State::find()->all(), 'id', 'name')
            ],
            [
                'attribute'=>'startdate',
                'value' => function($model){
                    return Yii::$app->formatter->asDate($model->startdate);
                }
            ],
            [
                'attribute'=>'enddate',
                'value' => function($model){
                    return Yii::$app->formatter->asDate($model->enddate);
                }
            ],
            // 'lga_id',
            // 'startdate',
            // 'enddate',
            // 'create_time',
            // 'create_user',
            // 'update_time',
            // 'update_user',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
