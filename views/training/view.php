<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use app\models\Resource;

/* @var $this yii\web\View */
/* @var $model app\models\Training */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Trainings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .training-resources-heading, .training-comments-heading{
        font-size: 20px;
    }
</style>
<div class="training-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if(Yii::$app->session->hasFlash('success_message')): ?>
    <div class="alert alert-info">
         <?= Yii::$app->session->getFlash('success_message') ?>
    </div>
    <?php endif; ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'title',
            [
                'attribute' => 'organizer',
                'value' => app\models\Organisation::getName($model->organizer)
            ],
            'description:ntext',
            'venue:ntext',
            [
                'attribute'=>'state_id',
                'value'=> $model->state->name
            ],
            [
                'attribute'=>'lga_id',
                'value'=> $model->lga->name
            ],
            [
                'attribute'=>'startdate',
                'value'=> Yii::$app->formatter->asDate($model->startdate)
            ],            
            [
                'attribute'=>'enddate',
                'value'=> Yii::$app->formatter->asDate($model->enddate)
            ],
            'expectations'
//            'create_time',
//            'create_user',
//            'update_time',
//            'update_user',
        ],
    ]) ?>

</div>
<br>

<div class="row">
    <div class="col-md-12">
        <span class="pull-left training-resources-heading"><strong>Training Resources</strong></span> <span class="pull-right"><?= Html::a('Add New Resource', ['/training-resource/create', 'id' => $model->id],['class' => 'btn btn-success']) ?></span>
    </div>
</div>
<br/>
<?php if(count($resources) == 0): ?>
    <div class="row">
        <div class="col-md-12">
            <h3>There are no training materials for this training</h3>
        </div>
    </div>
<?php else: ?>
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <tr>
                    <th>SN</th>
                    <th>Resource Category</th>
                    <th>Resource Name</th>
                    <th>Link</th>
                    <th>Action</th>
                </tr>
                <?php $sn=1; ?>
                <?php foreach($resources as $resource): ?>
                    <tr>
                        <td><?= $sn ?></td>
                        <td><?= \app\models\ResourceCategory::getName($resource->category_id) ?></td>
                        <td><?= $resource->name ?></td>
                        <td>
                            <?= Html::a('View / Download File', ['/resource-uploads/'.$resource->filename],
                                ['class' => 'btn btn-primary']) ?>
                        </td>
                        <td>
                            <?= "<a data-method='post' data-confirm='Are you sure you want to remove this resource?' class='btn btn-danger' href=/hworker/training-resource/delete?id=".$model->id."&resource_id=".$resource->id.">Remove / Delete</a>" ?>
                        </td>
                    </tr>
                <?php $sn++; ?>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
<?php endif; ?>

<br/>
<div class="row">
    <div class="col-md-12 training-comments-heading">Comments</div>
</div>
<div class="row">
    <?php if($comments): ?>
        <?php foreach ($comments as $comment):?>
            <div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Comment by <?= "<strong><em>".\app\models\User::findOne($comment->user_id)->username."</em></strong>" ?>
                    </div>
                    <div class="panel-body">
                        <?= $comment->comment ?>
                    </div>
                    <div class="panel-footer">
                        <span><?= Yii::$app->formatter->asDate($comment->create_time) ?></span>
                        <span class="col-md-offset-9">
                            <?php if(Yii::$app->user->getId() == $comment->user_id): ?>
                                <?= Html::a('Edit', ['/training-comment/update', 'id' => $comment->id ],['class' =>'btn btn-default']) ?>
                                <?= Html::a('Delete', ['/training-comment/delete', 'id' => $comment->id],
                                    [
                                        'class' =>'btn btn-default',
                                        'data' => [
                                            'confirm' => 'Are you sure you want to delete this item?',
                                            'method' => 'post',
                                        ],
                                    ]) ?>
                            <?php endif; ?>
                        </span>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php else: ?>
        <hr/>
        <div class="col-md-12"><h5>There are no comments for this training</h5></div>
    <?php endif; ?>
</div>
<div class="row">
    <div class="col-md-12"><?= Html::a('Add Comment', ['/training-comment/create', 'id'=> $model->id], ['class'=>'pull-right btn btn-success']) ?></div>
</div>

