<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;
use app\models\State;
use app\models\Lga;
use yii\web\View;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Training */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="training-form">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-6\">{input}</div>\n\n<div class=\"col-lg-2\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-2 control-label'],
        ],
    ]); ?>
    
    <?= $form->errorSummary([$model]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'organizer')->dropDownList(\app\models\Organisation::getOrganisationOptions(), ['prompt'=>'']) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'venue')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'state_id')->dropDownList(State::getStateOptions(['class'=>'form-control state-dropdown','id'=>'trainer-state_id'])) ?>

    <?= $form->field($model, 'lga_id')->dropDownList(Lga::getLgaOptions(['class'=>'form-control lga-dropdown','id'=>'trainer-lga_id'])) ?>

    <?= $form->field($model, 'expectations')->textarea() ?>
    
    <?= $form->field($model, 'startdate')->widget(DatePicker::className(),['dateFormat'=>'php:Y-m-d'], ['clientOptions' => ['dateFormat' => 'yy-mm-dd'],]) ?>

    <?= $form->field($model, 'enddate')->widget(DatePicker::className(),['dateFormat'=>'php:Y-m-d'], ['clientOptions' => ['dateFormat' => 'yy-mm-dd'],]) ?>
    
    
    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$this->registerJsFile(
    Url::base().'/web/js/lga_select.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);