<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Trainee */

$this->title = 'Directory - State Search';
$this->params['breadcrumbs'][] = ['label' => 'Trainers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="trainer-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <hr/>
    <?php if(isset($report)): ?>
    <?= $this->render('_state_report_table', [
        'trainers' => $trainers,
        'lga_count' => $lga_count,
        'state_id' => $state_id
    ]) ?>
    <?php else: ?>
    <?= $this->render('_state_report_form', [
        'model' => $model,
    ]) ?>
    <?php endif; ?>
    

</div>
