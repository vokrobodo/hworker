<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use app\libraries\Options;
use app\models\State;
use app\models\Lga;
use app\models\Organisation;
use app\models\Expertise;
use app\models\Training;
use yii\helpers\Url;
use app\models\Qualification;

/* @var $this yii\web\View */
/* @var $model app\models\Trainer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trainer-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'class'=>'ui form'],
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-6\">{input}</div>\n\n<div class=\"col-lg-2\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-2 control-label'],
        ],
        ]); ?>
    <?= $form->errorSummary([$model]) ?>

    <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'middlename')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true,'class'=>'form-control phone']) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gender')->dropDownList(Options::getGenderOptions(), ['prompt'=>'']) ?>

    <?= $form->field($model, 'state_id')->dropDownList(State::getStateOptions(), ['prompt'=>'']) ?>

    <?= $form->field($model, 'lga_id')->dropDownList(Lga::getLgaOptions(),['prompt'=>'']) ?>

    <?= $form->field($model, 'organisation_id')->dropDownList(Organisation::getOrganisationOptions(), ['prompt'=>'']) ?>
    
    <?php if($model->isNewRecord): ?>
        <?= $form->field($training, 'training_id[]')->dropDownList(Training::getTrainingOptions(),['multiple'=>'']) ?>
    <?php else: ?>
     <div class="form-group field-trainer-position required">
        <label class="col-lg-2 control-label" for="trainer-position">Position</label>
        <div class="col-lg-6">
        <?= 
            Html::activeDropDownList($train,'training_id[]', Training::getTrainingOptions(),
                    ['class'=>'form-control', 'multiple'=>'multiple', 'label'=>'Trainings', 'prompt'=>'Please Select', 'options'=>$training])?> 
        </div></div>
        <?php endif; ?>

    <?= $form->field($model, 'position')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'expertise_id')->dropDownList(Expertise::getExpertiseOptions()) ?>
    
    <?= $form->field($model, 'qualification_id')->dropDownList(Qualification::getQualifications()) ?>

    <?php /* $form->field($upload, 'photo')->fileInput()*/ ?>
    
    <?php /* $form->field($model, 'comment')->textarea()*/ ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
var cleave = new Cleave('.phone', {
    blocks: [4, 3, 4],
    uppercase: true
});
</script>
<?php
$this->registerJsFile(
    Url::base().'/web/js/lga_select.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
