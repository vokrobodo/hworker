<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use app\libraries\Options;
use app\models\Training;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Trainer */

$this->title = $model->surname." ".$model->firstname;
$this->params['breadcrumbs'][] = ['label' => 'Trainers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trainer-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if(Yii::$app->session->hasFlash('success_message')): ?>
    <div class="alert alert-info">
         <?= Yii::$app->session->getFlash('success_message') ?>
    </div>
    <?php endif; ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <div class="pull-right">
        <?php if($model->photo): ?>
            <?= Html::img(Url::base().'/photo-uploads/'.$model->photo, ['width'=>150, 'height'=>100]) ?> 
        <?php endif; ?>
    </div>
    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'ui table striped'],
        'attributes' => [
            //'id',
            'surname',
            'firstname',
            'middlename',
            [
                'attribute'=>'phone',
                'format'=>'html',
                'value'=>function($model){
                    $string = str_replace(' ', '', $model->phone);
                    return "<span class='phone'>$string</span>";
                }
            ],
            'email:email',
            [
                'attribute'=>'gender',
                'value'=> Options::getGenderText($model->gender)
            ],
            [
                'attribute'=>'state_id',
                'value'=> $model->state->name
            ],
            [
                'attribute'=>'lga_id',
                'value'=> $model->lga->name
            ],
            [
                'attribute'=>'organisation_id',
                'value'=> strtoupper($model->organisation->name)
            ],
            'position',
            //'photo',
            [
                'attribute'=>'expertise_id',
                'value'=> $model->expertise->name
            ],
            'comment'
//            'create_time',
//            'create_user',
//            'update_time',
//            'update_user',
        ],
    ]) ?>

    <?php //var_dump($trainings) ?>
    <hr/>
    <h2>Trainings Conducted</h2>
    <?php if(empty($training)):?>
    <div class="alert alert-info">No trainings available for this worker</div>
    <?php else: ?>
    <table class="table ui striped">
        <tr>
            <th>Training</th>
            <th>Description</th>
            <th>Date</th>
        </tr>
        <?php foreach($training as $t): ?>
        <tr>
            <td><?= Training::getTitle($t->training_id) ?></td>
            <td><?= Training::getDescription($t->training_id)?></td>
            <td><?= Training::getDates($t->training_id)?></td>
        </tr>
        <?php endforeach; ?>
    </table>
    <?php endif; ?>
</div>

<?php
$this->registerJsFile(
    Url::base().'/web/js/number-format.js',
    ['position' => View::POS_END]
);