<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use app\models\State;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $model app\models\Trainee */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trainee-form">
    
    
    <?php $form = ActiveForm::begin(['options' => ['class'=>'ui form'],
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-6\">{input}</div>\n\n<div class=\"col-lg-2\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-2 control-label'],
        ],
        ]); ?>
    
    <?= $form->errorSummary([$model]) ?>

    <?= $form->field($model, 'state_id')->dropDownList(State::getStateOptions(), ['prompt'=>'']) ?>
    <div class="form-group">
        <?= Html::submitButton('Search', ['class'=> 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    

</div>

