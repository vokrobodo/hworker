<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Trainee */

$this->title = 'Directory - LGA Search';
$this->params['breadcrumbs'][] = ['label' => 'Trainers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="trainee-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if(isset($report)): ?>
    <?= $this->render('_lga_report_table', [
        'trainers' => $trainers,
    ]) ?>
    <?php else: ?>
    <?= $this->render('_lga_report_form', [
        'model' => $model,
    ]) ?>
    <?php endif; ?>
    

</div>
