<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\View;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TrainerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Trainers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trainer-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Trainer', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'surname',
            'firstname',
            //'middlename',
            [
                'attribute'=>'organisation_id',
                'value'=>function($model){
                    return strtoupper($model->organisation->name);
                }
            ],
            [
                'attribute'=>'phone',
                'format'=>'html',
                'value'=>function($model){
                    $string = str_replace(' ', '', $model->phone);
                    return "<span class='phone'>$string</span>";
                }
            ],
            // 'email:email',
            // 'gender',
            [
                'attribute'=>'state_id',
                'value'=>function($model){
                    return $model->state->name;
                },
                'filter'=> \app\models\State::getStateOptions()
            ],
            // 'lga_id',
            
            // 'position',
            // 'photo',
            // 'expertise_id',
            // 'create_time',
            // 'create_user',
            // 'update_time',
            // 'update_user',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

<?php
$this->registerJsFile(
    Url::base().'/web/js/number-format.js',
    ['position' => View::POS_END]
);