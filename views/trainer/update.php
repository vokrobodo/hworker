<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Trainer */

$this->title = 'Update Trainer: ' . $model->surname." ".$model->firstname;
$this->params['breadcrumbs'][] = ['label' => 'Trainers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="trainer-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'training' => $training,
        'train' => $train,
        'upload' => $upload,
        'trainings' => $trainings
    ]) ?>

</div>
