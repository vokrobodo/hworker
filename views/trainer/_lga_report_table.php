<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Organisation;
use app\models\TrainerTraining;
?>

<?php if(empty($trainers)): ?>
<div class="alert alert-info">There are no trainees for the selected state</div>
<?php else: ?>
<!---------------------------------- show a card view of trainees --------------------------------------------->
<div class="row">
    <div class="col-md-6">
        <div class="ui statistic">
            <div class="value">
              <?= count($trainers)?>
            </div>
            <div class="label">
              Health Workers
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">Map Distribution</div>
            <div class="panel-body">
                <h1>MAP NOT AVAILABLE</h1>
            </div>
        </div>
    </div>
</div>
<hr/>

<div class="row">
    <?php foreach($trainers as $t): ?>
    <div class="col-md-3">
        <div class="ui card">
            <div class="image">
                <?php if(isset($t->photo)): ?>
                    <?= Html::img(Url::base().'/photo-uploads/'.$t->photo) ?>
                <?php else: ?>
                    <?= Html::img(Url::base().'/web/img/person1.jpg') ?>
                <?php endif; ?>
              
            </div>
            <div class="content">
              <a class="header"><?= ucwords(strtolower($t->surname. " ".$t->firstname))?></a>
              <div class="meta">
                <span class="date"><?= ucwords($t->position) ?></span>
              </div>
              <div class="description">
                <?= Organisation::getName($t->organisation_id)?>
              </div>
            </div>
            <div class="extra content">
              <span class="right floated">
                <?= Html::a('View Profile',['/trainee/view', 'id' => $t->id]) ?>
              </span>
              <span>
                <i class="user icon"></i>
                <?= TrainerTraining::getTrainingCount($t->id) ?> Training(s)
              </span>
            </div>
        </div>
        <br/>
    </div>
    <?php endforeach; ?>
    </div>
<?php endif; ?>

