<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Training */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Training Plans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="training-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if(Yii::$app->session->hasFlash('success_message')): ?>
    <div class="alert alert-info">
         <?= Yii::$app->session->getFlash('success_message') ?>
    </div>
    <?php endif; ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'title',
            'organizer',
            'description:ntext',
            'venue:ntext',
            [
                'attribute'=>'state_id',
                'value'=> $model->state->name
            ],
            [
                'attribute'=>'lga_id',
                'value'=> $model->lga->name
            ],
            [
                'attribute'=>'startdate',
                'value'=> Yii::$app->formatter->asDate($model->startdate)
            ],            
            [
                'attribute'=>'enddate',
                'value'=> Yii::$app->formatter->asDate($model->enddate)
            ],
            'expectations'
//            'create_time',
//            'create_user',
//            'update_time',
//            'update_user',
        ],
    ]) ?>

</div>
