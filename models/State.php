<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "state".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Healthfacility[] $healthfacilities
 * @property Lga[] $lgas
 * @property Trainee[] $trainees
 * @property Training[] $trainings
 * @property Ward[] $wards
 */
class State extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'state';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHealthfacilities()
    {
        return $this->hasMany(Healthfacility::className(), ['state_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLgas()
    {
        return $this->hasMany(Lga::className(), ['state_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainees()
    {
        return $this->hasMany(Trainee::className(), ['state_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainings()
    {
        return $this->hasMany(Training::className(), ['state_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWards()
    {
        return $this->hasMany(Ward::className(), ['state_id' => 'id']);
    }
    
    public static function getStateOptions()
    {
        return ArrayHelper::map(static::find()->all(), 'id', 'name');
    }
    
    public static function getStateName($id)
    {
        $state = static::findOne($id);
        return $state->name;
    }

    
}
