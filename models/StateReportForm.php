<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Description of StateReportForm
 *
 * @author Victor
 */
class StateReportForm extends Model{
    
    public $state_id;
    
    public function rules(){
        return[
           ['state_id', 'required'], 
        ];
    }
    
    public function attributeLabels() {
        return [
           'state_id' => 'State'  
        ];
    }
            
}