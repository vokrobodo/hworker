<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use yii\web\HttpException;

/**
 * Description of GalleryPhotoForm
 *
 * @author Victor
 */
class ResourceUploadForm extends Model{
    
    const SCENARIO_UPDATE = 'update';
    const SCENARIO_CREATE = 'create';
    const RESOURCE_FOLDER = 'resource-uploads/';
    /**
     * @var UploadedFile
     */
    public $file;
    public $dbName;

    public function rules()
    {
        return [
            [['file'], 'file', 'extensions' => 'png, jpg, jpeg, pdf, doc, docx, xls, xlsx, gif, ppt, pptx', 'maxSize'=>1024 * 1024 * 5],
            [['file'], 'required', 'on' => self::SCENARIO_CREATE ],
        ];
    }
    
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_UPDATE] = ['file'];
        $scenarios[self::SCENARIO_CREATE] = ['file'];
        return $scenarios;
    }
    
    public function upload()
    {
        if ($this->validate()) {
            $this->dbName = $this->generateNameString(). '.' . $this->file->extension;
            $this->file->saveAs(self::RESOURCE_FOLDER . $this->dbName);
            return true;
        } else {
            throw new HttpException(404, 'The file could not be uploaded, please try again');
        }
    }    
        
    private function generateNameString()
    {
        $name = Yii::$app->security->generateRandomString(10);
        return $name;
    }

}
