<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "healthfacility".
 *
 * @property integer $id
 * @property integer $state_id
 * @property integer $lga_id
 * @property integer $ward_id
 * @property string $name
 * @property string $address
 * @property integer $status
 * @property string $latitude
 * @property string $longitide
 * @property string $create_time
 * @property integer $create_user
 * @property string $update_time
 * @property integer $update_user
 *
 * @property State $state
 * @property Ward $ward
 * @property Lga $lga
 * @property Trainee[] $trainees
 */
class Healthfacility extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE=1;
    const STATUS_INACTIVE=2;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'healthfacility';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['state_id', 'lga_id', 'ward_id', 'status', 'create_user', 'update_user'], 'integer'],
            [['address'], 'string'],
            [['latitude', 'longitide'], 'number'],
            [['create_time', 'update_time'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['state_id'], 'exist', 'skipOnError' => true, 'targetClass' => State::className(), 'targetAttribute' => ['state_id' => 'id']],
            [['ward_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ward::className(), 'targetAttribute' => ['ward_id' => 'id']],
            [['lga_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lga::className(), 'targetAttribute' => ['lga_id' => 'id']],
            [['state_id', 'lga_id', 'status', 'name', 'address'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'state_id' => 'State',
            'lga_id' => 'Lga',
            'ward_id' => 'Ward',
            'name' => 'Name',
            'address' => 'Address',
            'status' => 'Status',
            'latitude' => 'Latitude',
            'longitide' => 'Longitide',
            'create_time' => 'Create Time',
            'create_user' => 'Create User',
            'update_time' => 'Update Time',
            'update_user' => 'Update User',
        ];
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_time',
                'updatedAtAttribute' => 'update_time',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'create_user',
                'updatedByAttribute' => 'update_user',
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(State::className(), ['id' => 'state_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWard()
    {
        return $this->hasOne(Ward::className(), ['id' => 'ward_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLga()
    {
        return $this->hasOne(Lga::className(), ['id' => 'lga_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainees()
    {
        return $this->hasMany(Trainee::className(), ['facility_id' => 'id']);
    }
    
    public static function getFacilityOptions()
    {
        return ArrayHelper::map(static::find()->all(), 'id', 'name');
    }
    
    public static function getStatusOptions()
    {
        return[
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_INACTIVE => 'Inactive'
        ];
    }
    
    public static function getStatusText($id)
    {
        if(!is_int($id)){
            return '';
        }
        $status = self::getStatusOptions();
        return $status[$id];
    }
    
    public static function getName($id)
    {
        if(!is_int($id)){
            return '';
        }
        $f = static::findOne($id);
        return $f->name;
    }
}
