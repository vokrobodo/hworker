<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "gallery".
 *
 * @property integer $id
 * @property string $title
 * @property string $shortdescription
 * @property string $longdescription
 * @property string $date
 * @property string $mainphoto
 * @property string $create_time
 * @property integer $create_user
 * @property string $update_time
 * @property integer $update_user
 *
 * @property GalleryPicture[] $galleryPictures
 */
class Gallery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gallery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shortdescription', 'longdescription'], 'string'],
            [['date', 'create_time', 'update_time'], 'safe'],
            [['create_user', 'update_user'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['title', 'shortdescription', 'longdescription', 'date'], 'required'],
            ['date', 'date', 'format'=>'yyyy-mm-dd'],
            ['date','compare','compareValue'=> date('Y-m-d'), 'operator' => '<=', 'enableClientValidation' => true,
                'message'=> 'Selected date must not be in the future']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'shortdescription' => 'Short description',
            'longdescription' => 'Long description',
            'date' => 'Date',
            'mainphoto' => 'Mainphoto',
            'create_time' => 'Create Time',
            'create_user' => 'Create User',
            'update_time' => 'Update Time',
            'update_user' => 'Update User',
        ];
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_time',
                'updatedAtAttribute' => 'update_time',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'create_user',
                'updatedByAttribute' => 'update_user',
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGalleryPictures()
    {
        return $this->hasMany(GalleryPicture::className(), ['gallery_id' => 'id']);
    }
    
    public static function getTitle($id)
    {
        $gallery = static::findOne($id);
        return $gallery->title;
    }
}
