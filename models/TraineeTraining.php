<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trainee_training".
 *
 * @property integer $id
 * @property integer $trainee_id
 * @property integer $training_id
 * @property integer $score
 *
 * @property Trainee $trainee
 * @property Training $training
 */
class TraineeTraining extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trainee_training';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['trainee_id', 'training_id'], 'integer'],
//            [['trainee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Trainee::className(), 'targetAttribute' => ['trainee_id' => 'id']],
//            [['training_id'], 'exist', 'skipOnError' => true, 'targetClass' => Training::className(), 'targetAttribute' => ['training_id' => 'id']],
            [['trainee_id','training_id','score', 'rating', 'facility_id'], 'integer'],
            [['trainee_id', 'training_id'], 'unique', 'targetAttribute' => ['trainee_id', 'training_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'trainee_id' => 'Trainee',
            'training_id' => 'Training attended',
            'score' => 'Score',
            'rating' => 'Rating',
            'facility_id' => 'Facility'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainee()
    {
        return $this->hasOne(Trainee::className(), ['id' => 'trainee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTraining()
    {
        return $this->hasOne(Training::className(), ['id' => 'training_id']);
    }
    
    public static function getTrainingCount($trainee_id)
    {
        $c = static::find()->where(['trainee_id' => $trainee_id])->count();
        return $c;
    }
}
