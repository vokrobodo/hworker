<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Trainee;

/**
 * TraineeSearch represents the model behind the search form about `app\models\Trainee`.
 */
class TraineeSearch extends Trainee
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'gender', 'state_id', 'lga_id', 'ward_id','facility_id', 'serviceyears'], 'integer'],
            [['surname', 'firstname', 'middlename', 'phone', 'email', 'position', 'photo', 'create_time', 'update_time',
                'latitude', 'longitude', 'comment', 'dateofbirth'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Trainee::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'gender' => $this->gender,
            'state_id' => $this->state_id,
            'lga_id' => $this->lga_id,
            'ward_id' => $this->ward_id,
            'facility_id' => $this->facility_id,
            'serviceyears' => $this->serviceyears,
            'create_time' => $this->create_time,
            'create_user' => $this->create_user,
            'update_time' => $this->update_time,
            'update_user' => $this->update_user,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
        ]);

        $query->andFilterWhere(['like', 'surname', $this->surname])
            ->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'middlename', $this->middlename])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'position', $this->position])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'dateofbirth', $this->dateofbirth])
                ;

        return $dataProvider;
    }
}
