<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use app\models\Training;
use yii\db\Query;

/**
 * This is the model class for table "trainee".
 *
 * @property integer $id
 * @property string $surname
 * @property string $firstname
 * @property string $middlename
 * @property string $phone
 * @property string $email
 * @property integer $gender
 * @property integer $state_id
 * @property integer $lga_id
 * @property integer $ward_id
 * @property integer $facility_id
 * @property integer $serviceyears
 * @property string $position
 * @property string $photo
 * @property string $create_time
 * @property integer $create_user
 * @property string $update_time
 * @property integer $update_user
 * @property number $latitude
 * @property number $longitude
 * @property string $comment
 * @property string $dateofbirth
 * @property Healthfacility $facility
 * @property Lga $lga
 * @property State $state
 */
class Trainee extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trainee';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gender', 'state_id', 'lga_id', 'facility_id','ward_id', 'serviceyears'], 'integer'],
            [['surname', 'firstname', 'middlename', 'phone', 'email', 'position'], 'string', 'max' => 255],
            [['facility_id'], 'exist', 'skipOnError' => true, 'targetClass' => Healthfacility::className(), 'targetAttribute' => ['facility_id' => 'id']],
            [['lga_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lga::className(), 'targetAttribute' => ['lga_id' => 'id']],
            [['state_id'], 'exist', 'skipOnError' => true, 'targetClass' => State::className(), 'targetAttribute' => ['state_id' => 'id']],
            [['surname', 'firstname', 'phone', 'email', 'position',
                'gender', 'state_id', 'lga_id', 'facility_id', 'serviceyears','dateofbirth'], 'required'],
            ['email', 'email'],
            [['latitude', 'longitude'], 'double'],
            //['comment','safe'],
            ['dateofbirth','date','format'=>'php:Y-m-d'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'surname' => 'Surname',
            'firstname' => 'First name',
            'middlename' => 'Middle name',
            'phone' => 'Phone',
            'email' => 'Email',
            'gender' => 'Gender',
            'state_id' => 'State of current deployment',
            'lga_id' => 'Local Government Area',
            'ward_id' => 'Ward',
            'facility_id' => 'Health Facility',
            'serviceyears' => 'Years left in service',
            'position' => 'Position',
            'photo' => 'Photo',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'comment' => 'Comments',
            'dateofbirth' => 'Date of birth',
            'create_time' => 'Create Time',
            'create_user' => 'Create User',
            'update_time' => 'Update Time',
            'update_user' => 'Update User',
        ];
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_time',
                'updatedAtAttribute' => 'update_time',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'create_user',
                'updatedByAttribute' => 'update_user',
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFacility()
    {
        return $this->hasOne(Healthfacility::className(), ['id' => 'facility_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLga()
    {
        return $this->hasOne(Lga::className(), ['id' => 'lga_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function geWard()
    {
        return $this->hasOne(Ward::className(), ['id' => 'ward_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(State::className(), ['id' => 'state_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHealthfacility()
    {
        return $this->hasOne(Healthfacility::className(), ['id' => 'facility_id']);
    }
    
    public function getTrainings()
    {
        return $this->hasMany(Training::className(), ['id' => 'training_id'])
            ->viaTable('trainee_training', ['trainee_id' => 'id']);
    }

    public static function getFullName($id)
    {
        $trainee = static::findOne($id);
        return ucfirst($trainee->surname." ".$trainee->firstname);
    }

    public static function getTrainingsByFacility($id)
    {
        $result = (new Query())
            ->select('training.id')->distinct()
            ->addSelect(['training.title', 'training.organizer', 'training.startdate', 'training.enddate', 'training.venue' ])
            ->from('training')
            ->join('INNER JOIN', 'trainee_training', 'training.id = trainee_training.training_id')
            ->join('INNER JOIN', 'trainee', 'trainee.id = trainee_training.trainee_id')
            ->join('INNER JOIN', 'healthfacility', 'healthfacility.id = trainee.facility_id')
            ->where('healthfacility.id = :facility_id', [':facility_id' => $id])
            ->orderBy('training.startdate')
            ->all();

        return $result;
    }

    public static function getTrainingCountByFacility($facility_id, $training_id)
    {
        $result = (new Query())
            ->select('training.id')
            ->from('training')
            ->join('INNER JOIN', 'trainee_training', 'training.id = trainee_training.training_id')
            ->join('INNER JOIN', 'trainee', 'trainee.id = trainee_training.trainee_id')
            ->join('INNER JOIN', 'healthfacility', 'healthfacility.id = trainee.facility_id')
            ->where('healthfacility.id = :facility_id', [':facility_id' => $facility_id])
            ->andWhere('training.id = :training', [':training' => $training_id])
            ->count();

        return $result;
    }

    public static function getTraineesByFacility($facility_id, $training_id)
    {
        $result = (new Query())
            ->select(['trainee.surname', 'trainee.firstname'])
            ->from('trainee')
            ->join('INNER JOIN', 'trainee_training', 'trainee.id = trainee_training.trainee_id')
            ->join('INNER JOIN', 'training', 'training.id = trainee_training.training_id')
            ->join('INNER JOIN', 'healthfacility', 'healthfacility.id = trainee.facility_id')
            ->where('healthfacility.id = :facility_id', [':facility_id' => $facility_id])
            ->andWhere('training.id = :training', [':training' => $training_id])
            ->orderBy('trainee.surname')
            ->all();

        return $result;
    }
}
