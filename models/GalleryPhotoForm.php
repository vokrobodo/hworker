<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use yii\web\HttpException;

/**
 * Description of GalleryPhotoForm
 *
 * @author Victor
 */
class GalleryPhotoForm extends Model{
    
    const SCENARIO_UPDATE = 'update';
    const SCENARIO_CREATE = 'create';
    const GALLERY_FOLDER = 'gallery-uploads/';
    /**
     * @var UploadedFile
     */
    public $photo;
    public $dbName;

    public function rules()
    {
        return [
            [['photo'], 'file', 'extensions' => 'png, jpg, jpeg', 'maxSize'=>1024 * 1024],
            [['photo'], 'required', 'on' => self::SCENARIO_CREATE ],
        ];
    }
    
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_UPDATE] = ['photo'];
        $scenarios[self::SCENARIO_CREATE] = ['photo'];
        return $scenarios;
    }
    
    public function upload()
    {
        if ($this->validate()) {
            $this->dbName = $this->generateNameString(). '.' . $this->photo->extension;
            $this->photo->saveAs(self::GALLERY_FOLDER . $this->dbName);
            return true;
        } else {
            throw new HttpException(404, 'The file could not be uploaded, please try again');
        }
    }
    
    public function uploadUpdate()
    {
        if($this->photo == NULL){//if no photo is uploaded, continue with saving other fields
            return true;
        }
            
        $this->upload();        
    }
    
    private function generateNameString()
    {
        $name = Yii::$app->security->generateRandomString(10);
        return $name;
    }

}
