<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trainer_training".
 *
 * @property integer $id
 * @property integer $trainer_id
 * @property integer $training_id
 *
 * @property Trainer $trainer
 * @property Training $training
 */
class TrainerTraining extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trainer_training';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['trainer_id', 'training_id'], 'integer'],
//            [['trainer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Trainer::className(), 'targetAttribute' => ['trainer_id' => 'id']],
//            [['training_id'], 'exist', 'skipOnError' => true, 'targetClass' => Training::className(), 'targetAttribute' => ['training_id' => 'id']],
            ['training_id', 'each', 'rule' => ['integer']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'trainer_id' => 'Trainer',
            'training_id' => 'Training Conducted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainer()
    {
        return $this->hasOne(Trainer::className(), ['id' => 'trainer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTraining()
    {
        return $this->hasOne(Training::className(), ['id' => 'training_id']);
    }
    
    public static function getTrainingCount($trainer_id)
    {
        $c = static::find()->where(['trainer_id' => $trainer_id])->count();
        return $c;
    }
}
