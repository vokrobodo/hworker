<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use app\models\Training;

/**
 * This is the model class for table "trainer".
 *
 * @property integer $id
 * @property string $surname
 * @property string $firstname
 * @property string $middlename
 * @property string $phone
 * @property string $email
 * @property integer $gender
 * @property integer $state_id
 * @property integer $lga_id
 * @property integer $organisation_id
 * @property string $position
 * @property string $photo
 * @property integer $expertise_id
 * @property integer $qualification_id
 * @property string $comment
 * @property string $create_time
 * @property integer $create_user
 * @property string $update_time
 * @property integer $update_user
 *
 * @property Expertise $expertise
 * @property Lga $lga
 * @property Organisation $organisation
 * @property State $state
 * @property TrainerTraining[] $trainerTrainings
 */
class Trainer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trainer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gender', 'state_id', 'lga_id', 'organisation_id','qualification_id', 'expertise_id', 'create_user', 'update_user'], 'integer'],
            //[['comment'], 'safe'],
            [['surname', 'firstname', 'middlename', 'phone', 'email', 'position'], 'string', 'max' => 255],
            [['expertise_id'], 'exist', 'skipOnError' => true, 'targetClass' => Expertise::className(), 'targetAttribute' => ['expertise_id' => 'id']],
            [['lga_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lga::className(), 'targetAttribute' => ['lga_id' => 'id']],
            [['organisation_id'], 'exist', 'skipOnError' => true, 'targetClass' => Organisation::className(), 'targetAttribute' => ['organisation_id' => 'id']],
            [['state_id'], 'exist', 'skipOnError' => true, 'targetClass' => State::className(), 'targetAttribute' => ['state_id' => 'id']],
            [['surname', 'firstname', 'phone', 'email', 'position',
                'gender', 'state_id', 'lga_id', 'organisation_id', 'expertise_id'], 'required'],
            ['email', 'email']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'surname' => 'Surname',
            'firstname' => 'First name',
            'middlename' => 'Middle name',
            'phone' => 'Phone',
            'email' => 'Email',
            'gender' => 'Gender',
            'state_id' => 'State of current deployment',
            'lga_id' => 'Local Government Area',
            'organisation_id' => 'Organisation',
            'position' => 'Position',
            'photo' => 'Photo',
            'comment' => 'Comments',
            'expertise_id' => 'Expertise',
            'qualification_id' => 'Qualification',
            'create_time' => 'Create Time',
            'create_user' => 'Create User',
            'update_time' => 'Update Time',
            'update_user' => 'Update User',
        ];
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_time',
                'updatedAtAttribute' => 'update_time',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'create_user',
                'updatedByAttribute' => 'update_user',
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpertise()
    {
        return $this->hasOne(Expertise::className(), ['id' => 'expertise_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLga()
    {
        return $this->hasOne(Lga::className(), ['id' => 'lga_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrganisation()
    {
        return $this->hasOne(Organisation::className(), ['id' => 'organisation_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(State::className(), ['id' => 'state_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainerTrainings()
    {
        return $this->hasMany(TrainerTraining::className(), ['trainer_id' => 'id']);
    }
}
