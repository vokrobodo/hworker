<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "ward".
 *
 * @property integer $id
 * @property integer $state_id
 * @property integer $lga_id
 * @property string $name
 *
 * @property Healthfacility[] $healthfacilities
 * @property Lga $lga
 * @property State $state
 */
class Ward extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ward';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['state_id', 'lga_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['lga_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lga::className(), 'targetAttribute' => ['lga_id' => 'id']],
            [['state_id'], 'exist', 'skipOnError' => true, 'targetClass' => State::className(), 'targetAttribute' => ['state_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'state_id' => 'State ID',
            'lga_id' => 'Lga ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHealthfacilities()
    {
        return $this->hasMany(Healthfacility::className(), ['ward_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLga()
    {
        return $this->hasOne(Lga::className(), ['id' => 'lga_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(State::className(), ['id' => 'state_id']);
    }
    
    public static function getWardOptions()
    {
        return ArrayHelper::map(static::find()->orderBy('name')->all(), 'id', 'name');
    }
    
    public static function getWardName($id)
    {
        $ward = static::findOne($id);
        return empty($ward)? "": $ward->name;
    }

    public static function getLgaOptions($lga_id)
    {
        return ArrayHelper::map(static::find()->where(['lga_id' => $lga_id])->all(), 'id', 'name');
    }
}
