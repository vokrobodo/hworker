<?php

namespace app\models;

use yii\base\Model;
/**
 * Description of LgaReportForm
 *
 * @author Victor
 */
class LgaReportForm extends Model{
    
    public $state_id;
    public $lga_id;
    
    public function rules(){
        return[
           [['state_id', 'lga_id'], 'required'], 
        ];
    }
    
    public function attributeLabels() {
        return [
           'state_id' => 'State',
            'lga_id' => 'LGA'
        ];
    }
}
