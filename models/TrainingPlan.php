<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "training_plan".
 *
 * @property integer $id
 * @property string $training_id
 * @property string $title
 * @property string $description
 * @property string $venue
 * @property integer $state_id
 * @property integer $lga_id
 * @property string $startdate
 * @property string $enddate
 * @property string $organizer
 * @property string $expectations
 * @property string $create_time
 * @property integer $create_user
 * @property string $update_time
 * @property integer $update_user
 *
 * @property TrainerTraining[] $trainerTrainings
 * @property Lga $lga
 * @property State $state
 */
class TrainingPlan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'training_plan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'venue','organizer'], 'string'],
            [['state_id', 'lga_id', 'create_user', 'update_user'], 'integer'],
            [['startdate', 'enddate', 'create_time', 'update_time','expectations'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['lga_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lga::className(), 'targetAttribute' => ['lga_id' => 'id']],
            [['state_id'], 'exist', 'skipOnError' => true, 'targetClass' => State::className(), 'targetAttribute' => ['state_id' => 'id']],
            [['title', 'description', 'venue', 'state_id', 'lga_id', 'startdate', 'enddate','organizer'], 'required'],
            [['startdate', 'enddate'], 'date', 'format'=>'yyyy-mm-dd'],
            ['startdate', 'compare', 'compareAttribute' => 'enddate', 'operator' => '<', 'enableClientValidation' => true],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'training_id' => 'Training ID',
            'title' => 'Title',
            'description' => 'Description',
            'venue' => 'Venue',
            'state_id' => 'State',
            'lga_id' => 'Local Government Area',
            'startdate' => 'Start date',
            'enddate' => 'End date',
            'organizer' => 'Conducted By',
            'expectations' => 'Training objectives',
            'create_time' => 'Create Time',
            'create_user' => 'Create User',
            'update_time' => 'Update Time',
            'update_user' => 'Update User',
        ];
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_time',
                'updatedAtAttribute' => 'update_time',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'create_user',
                'updatedByAttribute' => 'update_user',
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainerTrainings()
    {
        return $this->hasMany(TrainerTraining::className(), ['training_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLga()
    {
        return $this->hasOne(Lga::className(), ['id' => 'lga_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(State::className(), ['id' => 'state_id']);
    }
    
    public static function getTrainingOptions()
    {
        return ArrayHelper::map(static::find()->all(), 'id', 'title');
    }
    
    public static function getTitle($id)
    {
        $t = static::findOne($id);
        return $t->title;
    }
    
    public static function getDescription($id)
    {
        $t = static::findOne($id);
        return $t->description;
    }
    
    public static function getDates($id)
    {
        $t = static::findOne($id);
        return Yii::$app->formatter->asDate($t->startdate)." - ".Yii::$app->formatter->asDate($t->enddate);
    }
    
    public function generateTrainingCode()
    {
        $rand_int = rand(10000,99999);
        $string = "NPH-".$rand_int."-CDA";
        return $string;
    }
}
