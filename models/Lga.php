<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "lga".
 *
 * @property integer $id
 * @property integer $state_id
 * @property string $name
 *
 * @property Healthfacility[] $healthfacilities
 * @property State $state
 * @property Trainee[] $trainees
 * @property Training[] $trainings
 * @property Ward[] $wards
 */
class Lga extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lga';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['state_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['state_id'], 'exist', 'skipOnError' => true, 'targetClass' => State::className(), 'targetAttribute' => ['state_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'state_id' => 'State ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHealthfacilities()
    {
        return $this->hasMany(Healthfacility::className(), ['lga_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(State::className(), ['id' => 'state_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainees()
    {
        return $this->hasMany(Trainee::className(), ['lga_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainings()
    {
        return $this->hasMany(Training::className(), ['lga_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWards()
    {
        return $this->hasMany(Ward::className(), ['lga_id' => 'id']);
    }
    
    public static function getLgaOptions()
    {
        return ArrayHelper::map(static::find()->all(), 'id', 'name');
    }
    
    public static function getLgaName($id)
    {
        $lga = static::findOne($id);
        return $lga->name;
    }

    public static function getStateLgaOptions($state_id)
    {
        return ArrayHelper::map(static::find()->where(['state_id' => $state_id])->all(), 'id', 'name');
    }
}
