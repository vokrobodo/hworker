<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use yii\web\HttpException;

class PhotoUploadForm extends Model
{
    const SCENARIO_UPDATE = 'update';
    const SCENARIO_CREATE = 'create';
    /**
     * @var UploadedFile
     */
    public $photo;
    public $dbName;

    public function rules()
    {
        return [
            [['photo'], 'file', 'extensions' => 'png, jpg'],
            [['photo'], 'required', 'on' => self::SCENARIO_CREATE ],
        ];
    }
    
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_UPDATE] = ['photo'];
        $scenarios[self::SCENARIO_CREATE] = ['photo'];
        return $scenarios;
    }
    
    public function upload()
    {
        if ($this->validate()) {
            $this->dbName = $this->generateNameString(). '.' . $this->photo->extension;
            $this->photo->saveAs('photo-uploads/' . $this->dbName);            
            return true;
        } else {
            throw new HttpException(404, 'The file could not be uploaded, please try again');
        }
    }
    
    private function generateNameString()
    {
        $name = Yii::$app->security->generateRandomString(10);
        return $name;
    }
}
