<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $role
 * @property integer $state_id
 * @property string $create_time
 * @property integer $create_user
 * @property string $update_time
 * @property integer $update_user
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    const ROLE_ADMIN = 1;
    const ROLE_USER = 2;
    const ROLE_STATE_PARTNER  = 3;
    const ROLE_ORGANISATION = 4;
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
    
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';
    
    public $password_repeat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password', 'password_repeat'], 'string', 'max' => 255],
            ['email', 'email'],
            ['state_id', 'integer'],
            [['username', 'password', 'password_repeat'], 'trim'],
            [['username', 'password', 'password_repeat', 'role_id','email', 'status'], 'required'],
            ['password', 'compare', 'compareAttribute'=>'password_repeat', 'message'=>'Please enter the same password correctly'],
            ['username', 'unique'],
            ['state_id', 'required', 'when' => function($model){
                return $model->role_id == self::ROLE_STATE_PARTNER;
            }, 'whenClient' => "function (attribute, value) {
                return $('#state_id').val() == '3';
            }"]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'password_repeat' => 'Repeat Password',
            'role_id' => 'Role',
            'email' => 'Email',
            'state_id' => 'State',
            'status' => 'Status',
            'create_time' => 'Create Time',
            'create_user' => 'Create User',
            'update_time' => 'Update Time',
            'update_user' => 'Update User',
        ];
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_time',
                'updatedAtAttribute' => 'update_time',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'create_user',
                'updatedByAttribute' => 'update_user',
            ],
        ];
    }
    
    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->password = sha1($this->password);
            }
            return true;
        }
        return false;
    }
    
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE] = ['username', 'password', 'password_repeat','state_id', 'role_id','email','status'];
        $scenarios[self::SCENARIO_UPDATE] = ['username', 'password', 'role_id'];
        return $scenarios;
    }
    
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        //return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        //return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === sha1($password);
    }
    
    public static function getRoles()
    {
        return [
            self::ROLE_ADMIN => 'Admin',
            self::ROLE_USER => 'User',
            self::ROLE_STATE_PARTNER => 'State Partner',
            self::ROLE_ORGANISATION => 'Organization',
        ];
    }
    
    public static function getRole($id)
    {
        $roles = self::getRoles();
        return $roles[$id];
    }
    
    public static function getUserRole($id)
    {
        $u = static::findOne($id);
        return $u->role;
    }
    
    public static function getStatusOptions()
    {
        return[
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_INACTIVE => 'Inactive'
        ];
    }
    
    public static function getStatusText($id)
    {
        $status = self::getStatusOptions();
        return $status[$id];
    }

    public static function getLoggedInUserRole()
    {
        $id = Yii::$app->user->id;
        $u = static::findOne($id);
        return $u->role;
    }
    
    public static function isAdmin()
    {
        if(Yii::$app->user->isGuest){
            return FALSE;
        }
        $role = self::getLoggedInUserRole();
        if($role == self::ROLE_ADMIN){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    public static function isCustomer()
    {
        if(Yii::$app->user->isGuest){
            return FALSE;
        }
        $role = self::getLoggedInUserRole();
        if($role == self::ROLE_CUSTOMER){
            return TRUE;
        }else{
            return FALSE;
        }
    }


}