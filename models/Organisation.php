<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "organisation".
 *
 * @property integer $id
 * @property string $name
 * @property string $address
 * @property integer $state_id
 * @property integer $lga_id
 * @property integer $status
 * @property string $contact
 * property string $website
 * property string $specialization
 * property integer $type
 * @property string $create_time
 * @property integer $create_user
 * @property string $update_time
 * @property integer $update_user
 *
 * @property Lga $lga
 * @property State $state
 * @property Trainer[] $trainers
 */
class Organisation extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE=1;
    const STATUS_INACTIVE=2;
    const TYPE_PUBLIC=1;
    const TYPE_PRIVATE=2;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'organisation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address'], 'string'],
            [['state_id', 'lga_id', 'status', 'create_user', 'update_user'], 'integer'],
            [['create_time', 'update_time'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['lga_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lga::className(), 'targetAttribute' => ['lga_id' => 'id']],
            [['state_id'], 'exist', 'skipOnError' => true, 'targetClass' => State::className(), 'targetAttribute' => ['state_id' => 'id']],
            [['state_id', 'lga_id', 'name', 'address','contact', 'website', 'specialization'], 'required'],
            ['website','url']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'address' => 'Address',
            'state_id' => 'State',
            'lga_id' => 'Local Government Area',
            'status' => 'Status',
            'contact' =>'Contact',
            'website' => 'Web Address',
            'specialization' => 'Specialization',
            'type' => 'Type',
            'create_time' => 'Create Time',
            'create_user' => 'Create User',
            'update_time' => 'Update Time',
            'update_user' => 'Update User',
        ];
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_time',
                'updatedAtAttribute' => 'update_time',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'create_user',
                'updatedByAttribute' => 'update_user',
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLga()
    {
        return $this->hasOne(Lga::className(), ['id' => 'lga_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(State::className(), ['id' => 'state_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainers()
    {
        return $this->hasMany(Trainer::className(), ['organisation_id' => 'id']);
    }
    
    public static function getOrganisationOptions()
    {
        return ArrayHelper::map(static::find()->all(), 'id', 'name');
    }
    
    public static function getStatusOptions()
    {
        return[
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_INACTIVE => 'Inactive'
        ];
    }
    
    public static function getStatusText($id)
    {
        $status = self::getStatusOptions();
        return $status[$id];
    }
    
    public static function getTypeOptions()
    {
        return[
            self::TYPE_PUBLIC => 'Public',
            self::TYPE_PRIVATE => 'Private'
        ];
    }
    
    public static function getTypeText($id)
    {
        $types = self::getTypeOptions();
        return $types[$id];
    }
    
    public static function getName($id)
    {
        $org = static::findOne($id);
        $name = empty($org)? "" : $org->name;
        return $name;
    }
}
