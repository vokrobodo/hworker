<?php
namespace app\libraries;

/**
 * Description of Options
 *
 * @author Victor
 */
class Options {
    
    public static function getGenderOptions()
    {
        return[
            1 => 'Male',
            2 => 'Female'
        ];
    }
    
    public static function getGenderText($id)
    {
        if(!is_int($id)){
            return '';
        }
        $genders = self::getGenderOptions();
        return $genders[$id];
    }
}
