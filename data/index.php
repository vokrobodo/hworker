<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\web\View;
use app\models\Ward;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TraineeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Trainees';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trainee-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Trainee', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <hr/>
    <div class="row">
        <div class="col-md-6">
            <div class="ui statistics">
                <div class="statistic">
                  <div class="value">
                    <?= $lgacount?>
                  </div>
                  <div class="label">
                    LGAs Visited
                  </div>
                </div>
                <div class="statistic">
                  <div class="value">
                    <?= $ward_count ?>
                  </div>
                  <div class="label">
                    Wards Visited
                  </div>
                </div>
                <div class="statistic">
                  <div class="value">
                    <?= $trained ?>
                  </div>
                  <div class="label">
                    Trained Health Workers
                  </div>
                </div>
            </div>
            <hr/>
            <p id="homechart"></p>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">State Distribution</div>
                <div class="panel-body" id="map" style="height: 400px;">
                    
                </div>
            </div>
        </div>
    </div>
    <hr/>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute'=> 'surname',
                'value' => function($model){
                    return ucfirst(strtolower($model->surname));
                }
            ],
            [
                'attribute'=> 'firstname',
                'value' => function($model){
                    return ucfirst(strtolower($model->firstname));
                }
            ],
            //'middlename',
            [
                'attribute'=> 'ward_id',
                'value' => function($model){
                    return str_replace('?','',ucfirst(strtolower(Ward::getWardName($model->ward_id))));
                }
            ],
            [
                'attribute' => 'phone',
                'value' => function($model){
                    if(substr($model->phone, 0, 1) != 0){
                        return "0".$model->phone;
                    }
                    return $model->phone;
                }
            ],
            // 'email:email',
            // 'gender',
            // 'state_id',
            // 'lga_id',
            // 'facility_id',
            // 'serviceyears',
            // 'position',
            // 'photo',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>


<script>
    var cdata = JSON.parse('<?= $state_distribution;?>')
    //console.log(cdata);
</script>

<?php
$this->registerJsFile(
    Url::base().'/web/js/highcharts.js',
    ['position' => View::POS_HEAD]
);

$this->registerJs(
    "$(document).ready(function () {
    // Build the chart
    Highcharts.chart('homechart', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'State distribution of health workers'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y}</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Number',
            colorByPoint: true,            
            data: cdata
        }]
    });   
    
    
    
});",
    View::POS_READY,
    'my-button-handler'
);

?>


<script>
    function initMap(){
        
        var states = [
            'ottawa',
            'brussels',
            'stockholm'
        ];
        
        //var citymap = {};
        
        var citymap = {
        chicago: {
          center: {lat: 41.878, lng: -87.629},
          population: 2714856
        },
        newyork: {
          center: {lat: 40.714, lng: -74.005},
          population: 8405837
        },
        losangeles: {
          center: {lat: 34.052, lng: -118.243},
          population: 3857799
        },
        vancouver: {
          center: {lat: 49.25, lng: -123.1},
          population: 603502
        }
      };
        
                
        function geocodeAddress(address){
            var geocoder = new google.maps.Geocoder();
            var location = {};
            geocoder.geocode({'address': address}, function(results, status) {
                var coords = results[0].geometry.location.toJSON();
                location['center'] = coords;
                location['population'] = 1000000;
            });
            return location;
        }
        
        for(var i=0; i < states.length; i++){
            citymap[states[i]] = geocodeAddress(states[i]);
        }
        console.log(citymap);
        
        
        
        /************************** Map **********************************/
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 4,
          //center: {lat: 37.090, lng: -95.712},
          center: {lat: 9.0546462, lng: 7.2542664},
          mapTypeId: 'terrain'
        });

        // Construct the circle for each value in citymap.
        // Note: We scale the area of the circle based on the population.
        for (var city in citymap) {
          // Add the circle for this city to the map.
          var cityCircle = new google.maps.Circle({
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35,
            map: map,
            center: citymap[city].center,
            radius: Math.sqrt(citymap[city].population) * 100
          });
        }
        
        /*****************************************************************/
        
            
    }
    
    
            
</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBE5SaJX5exxAy3VeZX2E3uhL6hm68p4qg&callback=initMap">
</script>